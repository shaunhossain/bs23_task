# bs23_task

A simple Flutter application that shows the most starred GitHub repositories by
searching with the keyword "Flutter". Api documentation will found at [API DOC](https://docs.github.com/en/free-pro-team@latest/rest/search/search?apiVersion=2022-11-28#search-repositories)

## Screenshots

|<img src="screenshots/home_page.png" width=200/>|<img src="screenshots/detail_page.png" width=200/>|<img src="screenshots/retry_view.png" width=200/>|
|:----:|:----:|:----:|

## Generate Code 

If any auto-generated class file is missing just run the following command

```
flutter clean
flutter pub get
flutter pub run build_runner build --delete-conflicting-outputs
```

# Flutter requires SDK version
```
sdk: '>=3.1.3 <4.0.0'
```

# Folder Structure

```
├── lib
|   ├── presentation
│   │   ├── bloc
│   │   │   └── internet
|   |   |   └── repository_bloc
│   │   └── ui
│   │   |   ├── navigation
│   │   |   └── page
│   │   |   |   └── detail_page
│   │   |   |   └── home_page
|   |   |   └── widget
│   ├── data
|   |   └── api_service
|   |   └── db_service
|   |   └── repository
│   ├── domain
|   |   └── model
|   |   |   └── repository_response
|   |   |   └── error_response
|   |   |   └── dropdown_model
|   ├── util
│   └── main.dart
├── pubspec.lock
├── pubspec.yaml
```

## Architecture

we follow an architecture that has four layers.

- **Data layer:** This layer is the one in charge of interacting with APIs.
- **Domain layer:** This is the one in charge of transforming the data that comes from the data layer.
And finally, we want to manage the state of that data and present it on our user interface, that’s why we split the presentation layer in two:

- **Business logic layer:** This layer manages the state (usually using flutter_bloc).
- **Presentation layer:** Renders UI components based on state.
