
import 'package:bs23_task/domain/model/repository_response/repository_response.dart';

abstract class DbService {
  Future <List<Repository>> getAllRepositories();
  Future <void> insertRepository(Repository repository);
  Future <void> updateRepository(Repository repository);
  Future <void> deleteRepository(int id);
  Future <void> deleteAll();
}