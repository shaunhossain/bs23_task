import 'package:bs23_task/data/db_service/db_service.dart';
import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';

const dbName = 'repository-database';

@LazySingleton(as: DbService)
class RepositoryDbService extends DbService{
  @override
  Future<void> deleteAll() async {
    final box = await Hive.openBox<Repository>(dbName);
    box.clear();
  }

  @override
  Future<void> deleteRepository(int id) async {
    final box = await Hive.openBox<Repository>(dbName);
    await box.delete(id);
  }

  @override
  Future<List<Repository>> getAllRepositories() async {
    final box = await Hive.openBox<Repository>(dbName);
    return box.values.toList();
  }

  @override
  Future<void> insertRepository(Repository repository) async {
    final box = await Hive.openBox<Repository>(dbName);
    var newRepository = repository;
    final int id =await box.add(newRepository);
  }

  @override
  Future<void> updateRepository(Repository repository) async {
    final box = await Hive.openBox<Repository>(dbName);
    await box.put(repository.id, repository);
  }
}