import 'package:bs23_task/data/api_service/api_service.dart';
import 'package:bs23_task/data/db_service/db_service.dart';
import 'package:bs23_task/domain/model/error_response/error_response.dart';
import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

abstract class Repo {
  final ApiService apiService;
  final DbService dbService;

  Repo(this.apiService, this.dbService);
  Future<Either<ErrorResponse, List<RepositoryResponse>>> getAllSearchRepo(
      {required String query, required int page, required int perPage});
  Future<Either<ErrorResponse, List<RepositoryResponse>>>
      getAllSearchFilterRepo(
          {required String query,
          required String order,
          required String sort,
          required int page,
          required int perPage});
  Future<void> saveRepositoryLocally({
    required List<Repository> repositories,
  });
  Future<List<Repository>> getAllLocalRepositories();
  Future<void> clearAllLocalRepositories();
}

@LazySingleton(as: Repo)
class IRepo extends Repo {
  IRepo(super.apiService, super.dbService);

  @override
  Future<Either<ErrorResponse, List<RepositoryResponse>>> getAllSearchRepo(
      {required String query, required int page, required int perPage}) {
    return apiService.getAllSearchRepo(
        query: query, page: page, perPage: perPage);
  }

  @override
  Future<Either<ErrorResponse, List<RepositoryResponse>>>
      getAllSearchFilterRepo(
          {required String query,
          required String order,
          required String sort,
          required int page,
          required int perPage}) {
    return apiService.getAllSearchFilterRepo(
        query: query, order: order, sort: sort, page: page, perPage: perPage);
  }

  @override
  Future<List<Repository>> getAllLocalRepositories() async {
    return await dbService.getAllRepositories();
  }

  @override
  Future<void> saveRepositoryLocally(
      {required List<Repository> repositories}) async {
    for (final repository in repositories) {
      await dbService.insertRepository(repository);
    }
  }

  @override
  Future<void> clearAllLocalRepositories() async {
    await dbService.deleteAll();
  }
}
