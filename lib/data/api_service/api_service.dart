import 'package:bs23_task/domain/model/error_response/error_response.dart';
import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

abstract class ApiService {
  Dio client = Dio();

  Future<Either<ErrorResponse, List<RepositoryResponse>>> getAllSearchRepo(
      {required String query, required int page, required int perPage});
  Future<Either<ErrorResponse, List<RepositoryResponse>>>
      getAllSearchFilterRepo(
          {required String query,
          required String order,
          required String sort,
          required int page,
          required int perPage});
}
