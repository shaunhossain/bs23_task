import 'package:bs23_task/data/api_service/api_service.dart';
import 'package:bs23_task/domain/model/error_response/error_response.dart';
import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:bs23_task/util/endpoints/api_endpoints.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: ApiService)
class IApiService extends ApiService {
  ErrorResponse checkResponseError(DioException err) {
    if (err.type == DioExceptionType.badResponse) {
      var errorData = err.response?.data;
      var errorModel = ErrorResponse.fromJson(errorData);
      return errorModel;
    } else {
      return const ErrorResponse();
    }
  }

  @override
  Future<Either<ErrorResponse, List<RepositoryResponse>>> getAllSearchRepo(
      {required String query, required int page, required int perPage}) async {
    try {
      var headers = {
        'Authorization':
            'Bearer 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',
        'Accept': 'application/json'
      };
      Response response = await client.get(
          ApiEndpoints.searchRepositoriesUrl(
              query: query, page: page, perPage: perPage),
          options: Options(headers: headers));
      var result = RepositoryResponse.fromJson(response.data);
      return right([result]);
    } on DioException catch (e) {
      return left(checkResponseError(e));
    }
  }

  @override
  Future<Either<ErrorResponse, List<RepositoryResponse>>>
      getAllSearchFilterRepo(
          {required String query,
          required String order,
          required String sort,
          required int page,
          required int perPage}) async {
    try {
      var headers = {
        'Authorization':
            'Bearer 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',
        'Accept': 'application/json'
      };
      Response response = await client.get(
          ApiEndpoints.searchWithFilterRepositoriesUrl(
              query: query,
              page: page,
              perPage: perPage,
              order: order,
              sort: sort),
          options: Options(headers: headers));
      var result = RepositoryResponse.fromJson(response.data);
      return right([result]);
    } on DioException catch (e) {
      return left(checkResponseError(e));
    }
  }
}
