class DropdownModel {
  DropdownModel({required this.title, required this.order, required this.sort});
  final String title;
  final String order;
  final String sort;
}
