import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

part 'repository_response.freezed.dart';
part 'repository_response.g.dart';

@freezed
@HiveType(typeId: 0 ,
    adapterName: 'RepositoryResponseAdapter')
class RepositoryResponse with _$RepositoryResponse {
  const factory RepositoryResponse({
    @HiveField(0)
    @JsonKey(name: "total_count")
    int? totalCount,

    @HiveField(1)
    @JsonKey(name: "incomplete_results")
    bool? incompleteResults,

    @HiveField(2)
    @JsonKey(name: "items")
    List<Repository>? items,

  }) = _RepositoryResponse;

  factory RepositoryResponse.fromJson(Map<String,
      dynamic> json) => _$RepositoryResponseFromJson(json);
}

@freezed
@HiveType(typeId: 1 ,
    adapterName: 'RepositoryAdapter')
class Repository with _$Repository {
  const factory Repository({
    @HiveField(0)
    @JsonKey(name: "id")
    int? id,

    @HiveField(1)
    @JsonKey(name: "node_id")
    String? nodeId,

    @HiveField(3)
    @JsonKey(name: "name")
    String? name,

    @HiveField(4)
    @JsonKey(name: "full_name")
    String? fullName,

    @HiveField(5)
    @JsonKey(name: "private")
    bool? private,

    @HiveField(6)
    @JsonKey(name: "owner")
    Owner? owner,

    @HiveField(7)
    @JsonKey(name: "html_url")
    String? htmlUrl,

    @HiveField(8)
    @JsonKey(name: "description")
    String? description,

    @HiveField(9)
    @JsonKey(name: "fork")
    bool? fork,

    @HiveField(10)
    @JsonKey(name: "created_at")
    DateTime? createdAt,

    @HiveField(11)
    @JsonKey(name: "updated_at")
    DateTime? updatedAt,

    @HiveField(12)
    @JsonKey(name: "pushed_at")
    DateTime? pushedAt,

    @HiveField(13)
    @JsonKey(name: "size")
    int? size,

    @HiveField(14)
    @JsonKey(name: "stargazers_count")
    int? stargazersCount,

    @HiveField(15)
    @JsonKey(name: "watchers_count")
    int? watchersCount,

    @HiveField(16)
    @JsonKey(name: "language")
    String? language,

    @HiveField(17)
    @JsonKey(name: "has_issues")
    bool? hasIssues,

    @HiveField(18)
    @JsonKey(name: "has_projects")
    bool? hasProjects,

    @HiveField(19)
    @JsonKey(name: "has_downloads")
    bool? hasDownloads,

    @HiveField(20)
    @JsonKey(name: "has_wiki")
    bool? hasWiki,

    @HiveField(21)
    @JsonKey(name: "has_pages")
    bool? hasPages,

    @HiveField(22)
    @JsonKey(name: "has_discussions")
    bool? hasDiscussions,

    @HiveField(23)
    @JsonKey(name: "forks_count")
    int? forksCount,

    @HiveField(24)
    @JsonKey(name: "open_issues_count")
    int? openIssuesCount,

    @HiveField(25)
    @JsonKey(name: "license")
    License? license,

    @HiveField(26)
    @JsonKey(name: "allow_forking")
    bool? allowForking,

    @HiveField(27)
    @JsonKey(name: "topics")
    List<String>? topics,

    @HiveField(28)
    @JsonKey(name: "visibility")
    String? visibility,

    @HiveField(29)
    @JsonKey(name: "forks")
    int? forks,

    @HiveField(30)
    @JsonKey(name: "open_issues")
    int? openIssues,

    @HiveField(31)
    @JsonKey(name: "watchers")
    int? watchers,

    @HiveField(32)
    @JsonKey(name: "default_branch")
    String? defaultBranch,

    @HiveField(33)
    @JsonKey(name: "score")
    double? score,

  }) = _Repository;

  factory Repository.fromJson(Map<String,
      dynamic> json) => _$RepositoryFromJson(json);
}

@freezed
@HiveType(typeId: 2 ,
    adapterName: 'LicenseAdapter')
class License with _$License {
  const factory License({
    @HiveField(0)
    @JsonKey(name: "key")
    String? key,

    @HiveField(1)
    @JsonKey(name: "name")
    String? name,

    @HiveField(2)
    @JsonKey(name: "spdx_id")
    String? spdxId,

    @HiveField(3)
    @JsonKey(name: "url")
    String? url,

    @HiveField(4)
    @JsonKey(name: "node_id")
    String? nodeId,

  }) = _License;

  factory License.fromJson(Map<String,
      dynamic> json) => _$LicenseFromJson(json);
}

@freezed
@HiveType(typeId: 3 ,
    adapterName: 'OwnerAdapter')
class Owner with _$Owner {
  const factory Owner({
    @HiveField(0)
    @JsonKey(name: "login")
    String? login,

    @HiveField(1)
    @JsonKey(name: "id")
    int? id,

    @HiveField(2)
    @JsonKey(name: "node_id")
    String? nodeId,

    @HiveField(3)
    @JsonKey(name: "avatar_url")
    String? avatarUrl,

    @HiveField(4)
    @JsonKey(name: "gravatar_id")
    String? gravatarId,

    @HiveField(5)
    @JsonKey(name: "url")
    String? url,

    @HiveField(6)
    @JsonKey(name: "html_url")
    String? htmlUrl,

    @HiveField(7)
    @JsonKey(name: "type")
    String? type,

    @HiveField(8)
    @JsonKey(name: "site_admin")
    bool? siteAdmin,

  }) = _Owner;

  factory Owner.fromJson(Map<String,
      dynamic> json) => _$OwnerFromJson(json);
}
