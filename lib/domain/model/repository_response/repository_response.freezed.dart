// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'repository_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RepositoryResponse _$RepositoryResponseFromJson(Map<String, dynamic> json) {
  return _RepositoryResponse.fromJson(json);
}

/// @nodoc
mixin _$RepositoryResponse {
  @HiveField(0)
  @JsonKey(name: "total_count")
  int? get totalCount => throw _privateConstructorUsedError;
  @HiveField(1)
  @JsonKey(name: "incomplete_results")
  bool? get incompleteResults => throw _privateConstructorUsedError;
  @HiveField(2)
  @JsonKey(name: "items")
  List<Repository>? get items => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RepositoryResponseCopyWith<RepositoryResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryResponseCopyWith<$Res> {
  factory $RepositoryResponseCopyWith(
          RepositoryResponse value, $Res Function(RepositoryResponse) then) =
      _$RepositoryResponseCopyWithImpl<$Res, RepositoryResponse>;
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "total_count") int? totalCount,
      @HiveField(1)
      @JsonKey(name: "incomplete_results")
      bool? incompleteResults,
      @HiveField(2) @JsonKey(name: "items") List<Repository>? items});
}

/// @nodoc
class _$RepositoryResponseCopyWithImpl<$Res, $Val extends RepositoryResponse>
    implements $RepositoryResponseCopyWith<$Res> {
  _$RepositoryResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalCount = freezed,
    Object? incompleteResults = freezed,
    Object? items = freezed,
  }) {
    return _then(_value.copyWith(
      totalCount: freezed == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int?,
      incompleteResults: freezed == incompleteResults
          ? _value.incompleteResults
          : incompleteResults // ignore: cast_nullable_to_non_nullable
              as bool?,
      items: freezed == items
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Repository>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RepositoryResponseImplCopyWith<$Res>
    implements $RepositoryResponseCopyWith<$Res> {
  factory _$$RepositoryResponseImplCopyWith(_$RepositoryResponseImpl value,
          $Res Function(_$RepositoryResponseImpl) then) =
      __$$RepositoryResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "total_count") int? totalCount,
      @HiveField(1)
      @JsonKey(name: "incomplete_results")
      bool? incompleteResults,
      @HiveField(2) @JsonKey(name: "items") List<Repository>? items});
}

/// @nodoc
class __$$RepositoryResponseImplCopyWithImpl<$Res>
    extends _$RepositoryResponseCopyWithImpl<$Res, _$RepositoryResponseImpl>
    implements _$$RepositoryResponseImplCopyWith<$Res> {
  __$$RepositoryResponseImplCopyWithImpl(_$RepositoryResponseImpl _value,
      $Res Function(_$RepositoryResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalCount = freezed,
    Object? incompleteResults = freezed,
    Object? items = freezed,
  }) {
    return _then(_$RepositoryResponseImpl(
      totalCount: freezed == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int?,
      incompleteResults: freezed == incompleteResults
          ? _value.incompleteResults
          : incompleteResults // ignore: cast_nullable_to_non_nullable
              as bool?,
      items: freezed == items
          ? _value._items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Repository>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RepositoryResponseImpl implements _RepositoryResponse {
  const _$RepositoryResponseImpl(
      {@HiveField(0) @JsonKey(name: "total_count") this.totalCount,
      @HiveField(1) @JsonKey(name: "incomplete_results") this.incompleteResults,
      @HiveField(2) @JsonKey(name: "items") final List<Repository>? items})
      : _items = items;

  factory _$RepositoryResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$RepositoryResponseImplFromJson(json);

  @override
  @HiveField(0)
  @JsonKey(name: "total_count")
  final int? totalCount;
  @override
  @HiveField(1)
  @JsonKey(name: "incomplete_results")
  final bool? incompleteResults;
  final List<Repository>? _items;
  @override
  @HiveField(2)
  @JsonKey(name: "items")
  List<Repository>? get items {
    final value = _items;
    if (value == null) return null;
    if (_items is EqualUnmodifiableListView) return _items;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'RepositoryResponse(totalCount: $totalCount, incompleteResults: $incompleteResults, items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RepositoryResponseImpl &&
            (identical(other.totalCount, totalCount) ||
                other.totalCount == totalCount) &&
            (identical(other.incompleteResults, incompleteResults) ||
                other.incompleteResults == incompleteResults) &&
            const DeepCollectionEquality().equals(other._items, _items));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, totalCount, incompleteResults,
      const DeepCollectionEquality().hash(_items));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RepositoryResponseImplCopyWith<_$RepositoryResponseImpl> get copyWith =>
      __$$RepositoryResponseImplCopyWithImpl<_$RepositoryResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RepositoryResponseImplToJson(
      this,
    );
  }
}

abstract class _RepositoryResponse implements RepositoryResponse {
  const factory _RepositoryResponse(
      {@HiveField(0) @JsonKey(name: "total_count") final int? totalCount,
      @HiveField(1)
      @JsonKey(name: "incomplete_results")
      final bool? incompleteResults,
      @HiveField(2)
      @JsonKey(name: "items")
      final List<Repository>? items}) = _$RepositoryResponseImpl;

  factory _RepositoryResponse.fromJson(Map<String, dynamic> json) =
      _$RepositoryResponseImpl.fromJson;

  @override
  @HiveField(0)
  @JsonKey(name: "total_count")
  int? get totalCount;
  @override
  @HiveField(1)
  @JsonKey(name: "incomplete_results")
  bool? get incompleteResults;
  @override
  @HiveField(2)
  @JsonKey(name: "items")
  List<Repository>? get items;
  @override
  @JsonKey(ignore: true)
  _$$RepositoryResponseImplCopyWith<_$RepositoryResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

Repository _$RepositoryFromJson(Map<String, dynamic> json) {
  return _Repository.fromJson(json);
}

/// @nodoc
mixin _$Repository {
  @HiveField(0)
  @JsonKey(name: "id")
  int? get id => throw _privateConstructorUsedError;
  @HiveField(1)
  @JsonKey(name: "node_id")
  String? get nodeId => throw _privateConstructorUsedError;
  @HiveField(3)
  @JsonKey(name: "name")
  String? get name => throw _privateConstructorUsedError;
  @HiveField(4)
  @JsonKey(name: "full_name")
  String? get fullName => throw _privateConstructorUsedError;
  @HiveField(5)
  @JsonKey(name: "private")
  bool? get private => throw _privateConstructorUsedError;
  @HiveField(6)
  @JsonKey(name: "owner")
  Owner? get owner => throw _privateConstructorUsedError;
  @HiveField(7)
  @JsonKey(name: "html_url")
  String? get htmlUrl => throw _privateConstructorUsedError;
  @HiveField(8)
  @JsonKey(name: "description")
  String? get description => throw _privateConstructorUsedError;
  @HiveField(9)
  @JsonKey(name: "fork")
  bool? get fork => throw _privateConstructorUsedError;
  @HiveField(10)
  @JsonKey(name: "created_at")
  DateTime? get createdAt => throw _privateConstructorUsedError;
  @HiveField(11)
  @JsonKey(name: "updated_at")
  DateTime? get updatedAt => throw _privateConstructorUsedError;
  @HiveField(12)
  @JsonKey(name: "pushed_at")
  DateTime? get pushedAt => throw _privateConstructorUsedError;
  @HiveField(13)
  @JsonKey(name: "size")
  int? get size => throw _privateConstructorUsedError;
  @HiveField(14)
  @JsonKey(name: "stargazers_count")
  int? get stargazersCount => throw _privateConstructorUsedError;
  @HiveField(15)
  @JsonKey(name: "watchers_count")
  int? get watchersCount => throw _privateConstructorUsedError;
  @HiveField(16)
  @JsonKey(name: "language")
  String? get language => throw _privateConstructorUsedError;
  @HiveField(17)
  @JsonKey(name: "has_issues")
  bool? get hasIssues => throw _privateConstructorUsedError;
  @HiveField(18)
  @JsonKey(name: "has_projects")
  bool? get hasProjects => throw _privateConstructorUsedError;
  @HiveField(19)
  @JsonKey(name: "has_downloads")
  bool? get hasDownloads => throw _privateConstructorUsedError;
  @HiveField(20)
  @JsonKey(name: "has_wiki")
  bool? get hasWiki => throw _privateConstructorUsedError;
  @HiveField(21)
  @JsonKey(name: "has_pages")
  bool? get hasPages => throw _privateConstructorUsedError;
  @HiveField(22)
  @JsonKey(name: "has_discussions")
  bool? get hasDiscussions => throw _privateConstructorUsedError;
  @HiveField(23)
  @JsonKey(name: "forks_count")
  int? get forksCount => throw _privateConstructorUsedError;
  @HiveField(24)
  @JsonKey(name: "open_issues_count")
  int? get openIssuesCount => throw _privateConstructorUsedError;
  @HiveField(25)
  @JsonKey(name: "license")
  License? get license => throw _privateConstructorUsedError;
  @HiveField(26)
  @JsonKey(name: "allow_forking")
  bool? get allowForking => throw _privateConstructorUsedError;
  @HiveField(27)
  @JsonKey(name: "topics")
  List<String>? get topics => throw _privateConstructorUsedError;
  @HiveField(28)
  @JsonKey(name: "visibility")
  String? get visibility => throw _privateConstructorUsedError;
  @HiveField(29)
  @JsonKey(name: "forks")
  int? get forks => throw _privateConstructorUsedError;
  @HiveField(30)
  @JsonKey(name: "open_issues")
  int? get openIssues => throw _privateConstructorUsedError;
  @HiveField(31)
  @JsonKey(name: "watchers")
  int? get watchers => throw _privateConstructorUsedError;
  @HiveField(32)
  @JsonKey(name: "default_branch")
  String? get defaultBranch => throw _privateConstructorUsedError;
  @HiveField(33)
  @JsonKey(name: "score")
  double? get score => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RepositoryCopyWith<Repository> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryCopyWith<$Res> {
  factory $RepositoryCopyWith(
          Repository value, $Res Function(Repository) then) =
      _$RepositoryCopyWithImpl<$Res, Repository>;
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "id") int? id,
      @HiveField(1) @JsonKey(name: "node_id") String? nodeId,
      @HiveField(3) @JsonKey(name: "name") String? name,
      @HiveField(4) @JsonKey(name: "full_name") String? fullName,
      @HiveField(5) @JsonKey(name: "private") bool? private,
      @HiveField(6) @JsonKey(name: "owner") Owner? owner,
      @HiveField(7) @JsonKey(name: "html_url") String? htmlUrl,
      @HiveField(8) @JsonKey(name: "description") String? description,
      @HiveField(9) @JsonKey(name: "fork") bool? fork,
      @HiveField(10) @JsonKey(name: "created_at") DateTime? createdAt,
      @HiveField(11) @JsonKey(name: "updated_at") DateTime? updatedAt,
      @HiveField(12) @JsonKey(name: "pushed_at") DateTime? pushedAt,
      @HiveField(13) @JsonKey(name: "size") int? size,
      @HiveField(14) @JsonKey(name: "stargazers_count") int? stargazersCount,
      @HiveField(15) @JsonKey(name: "watchers_count") int? watchersCount,
      @HiveField(16) @JsonKey(name: "language") String? language,
      @HiveField(17) @JsonKey(name: "has_issues") bool? hasIssues,
      @HiveField(18) @JsonKey(name: "has_projects") bool? hasProjects,
      @HiveField(19) @JsonKey(name: "has_downloads") bool? hasDownloads,
      @HiveField(20) @JsonKey(name: "has_wiki") bool? hasWiki,
      @HiveField(21) @JsonKey(name: "has_pages") bool? hasPages,
      @HiveField(22) @JsonKey(name: "has_discussions") bool? hasDiscussions,
      @HiveField(23) @JsonKey(name: "forks_count") int? forksCount,
      @HiveField(24) @JsonKey(name: "open_issues_count") int? openIssuesCount,
      @HiveField(25) @JsonKey(name: "license") License? license,
      @HiveField(26) @JsonKey(name: "allow_forking") bool? allowForking,
      @HiveField(27) @JsonKey(name: "topics") List<String>? topics,
      @HiveField(28) @JsonKey(name: "visibility") String? visibility,
      @HiveField(29) @JsonKey(name: "forks") int? forks,
      @HiveField(30) @JsonKey(name: "open_issues") int? openIssues,
      @HiveField(31) @JsonKey(name: "watchers") int? watchers,
      @HiveField(32) @JsonKey(name: "default_branch") String? defaultBranch,
      @HiveField(33) @JsonKey(name: "score") double? score});

  $OwnerCopyWith<$Res>? get owner;
  $LicenseCopyWith<$Res>? get license;
}

/// @nodoc
class _$RepositoryCopyWithImpl<$Res, $Val extends Repository>
    implements $RepositoryCopyWith<$Res> {
  _$RepositoryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? nodeId = freezed,
    Object? name = freezed,
    Object? fullName = freezed,
    Object? private = freezed,
    Object? owner = freezed,
    Object? htmlUrl = freezed,
    Object? description = freezed,
    Object? fork = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? pushedAt = freezed,
    Object? size = freezed,
    Object? stargazersCount = freezed,
    Object? watchersCount = freezed,
    Object? language = freezed,
    Object? hasIssues = freezed,
    Object? hasProjects = freezed,
    Object? hasDownloads = freezed,
    Object? hasWiki = freezed,
    Object? hasPages = freezed,
    Object? hasDiscussions = freezed,
    Object? forksCount = freezed,
    Object? openIssuesCount = freezed,
    Object? license = freezed,
    Object? allowForking = freezed,
    Object? topics = freezed,
    Object? visibility = freezed,
    Object? forks = freezed,
    Object? openIssues = freezed,
    Object? watchers = freezed,
    Object? defaultBranch = freezed,
    Object? score = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nodeId: freezed == nodeId
          ? _value.nodeId
          : nodeId // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      fullName: freezed == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      private: freezed == private
          ? _value.private
          : private // ignore: cast_nullable_to_non_nullable
              as bool?,
      owner: freezed == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Owner?,
      htmlUrl: freezed == htmlUrl
          ? _value.htmlUrl
          : htmlUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      fork: freezed == fork
          ? _value.fork
          : fork // ignore: cast_nullable_to_non_nullable
              as bool?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      pushedAt: freezed == pushedAt
          ? _value.pushedAt
          : pushedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      size: freezed == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int?,
      stargazersCount: freezed == stargazersCount
          ? _value.stargazersCount
          : stargazersCount // ignore: cast_nullable_to_non_nullable
              as int?,
      watchersCount: freezed == watchersCount
          ? _value.watchersCount
          : watchersCount // ignore: cast_nullable_to_non_nullable
              as int?,
      language: freezed == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      hasIssues: freezed == hasIssues
          ? _value.hasIssues
          : hasIssues // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasProjects: freezed == hasProjects
          ? _value.hasProjects
          : hasProjects // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasDownloads: freezed == hasDownloads
          ? _value.hasDownloads
          : hasDownloads // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasWiki: freezed == hasWiki
          ? _value.hasWiki
          : hasWiki // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasPages: freezed == hasPages
          ? _value.hasPages
          : hasPages // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasDiscussions: freezed == hasDiscussions
          ? _value.hasDiscussions
          : hasDiscussions // ignore: cast_nullable_to_non_nullable
              as bool?,
      forksCount: freezed == forksCount
          ? _value.forksCount
          : forksCount // ignore: cast_nullable_to_non_nullable
              as int?,
      openIssuesCount: freezed == openIssuesCount
          ? _value.openIssuesCount
          : openIssuesCount // ignore: cast_nullable_to_non_nullable
              as int?,
      license: freezed == license
          ? _value.license
          : license // ignore: cast_nullable_to_non_nullable
              as License?,
      allowForking: freezed == allowForking
          ? _value.allowForking
          : allowForking // ignore: cast_nullable_to_non_nullable
              as bool?,
      topics: freezed == topics
          ? _value.topics
          : topics // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      visibility: freezed == visibility
          ? _value.visibility
          : visibility // ignore: cast_nullable_to_non_nullable
              as String?,
      forks: freezed == forks
          ? _value.forks
          : forks // ignore: cast_nullable_to_non_nullable
              as int?,
      openIssues: freezed == openIssues
          ? _value.openIssues
          : openIssues // ignore: cast_nullable_to_non_nullable
              as int?,
      watchers: freezed == watchers
          ? _value.watchers
          : watchers // ignore: cast_nullable_to_non_nullable
              as int?,
      defaultBranch: freezed == defaultBranch
          ? _value.defaultBranch
          : defaultBranch // ignore: cast_nullable_to_non_nullable
              as String?,
      score: freezed == score
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as double?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $OwnerCopyWith<$Res>? get owner {
    if (_value.owner == null) {
      return null;
    }

    return $OwnerCopyWith<$Res>(_value.owner!, (value) {
      return _then(_value.copyWith(owner: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $LicenseCopyWith<$Res>? get license {
    if (_value.license == null) {
      return null;
    }

    return $LicenseCopyWith<$Res>(_value.license!, (value) {
      return _then(_value.copyWith(license: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$RepositoryImplCopyWith<$Res>
    implements $RepositoryCopyWith<$Res> {
  factory _$$RepositoryImplCopyWith(
          _$RepositoryImpl value, $Res Function(_$RepositoryImpl) then) =
      __$$RepositoryImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "id") int? id,
      @HiveField(1) @JsonKey(name: "node_id") String? nodeId,
      @HiveField(3) @JsonKey(name: "name") String? name,
      @HiveField(4) @JsonKey(name: "full_name") String? fullName,
      @HiveField(5) @JsonKey(name: "private") bool? private,
      @HiveField(6) @JsonKey(name: "owner") Owner? owner,
      @HiveField(7) @JsonKey(name: "html_url") String? htmlUrl,
      @HiveField(8) @JsonKey(name: "description") String? description,
      @HiveField(9) @JsonKey(name: "fork") bool? fork,
      @HiveField(10) @JsonKey(name: "created_at") DateTime? createdAt,
      @HiveField(11) @JsonKey(name: "updated_at") DateTime? updatedAt,
      @HiveField(12) @JsonKey(name: "pushed_at") DateTime? pushedAt,
      @HiveField(13) @JsonKey(name: "size") int? size,
      @HiveField(14) @JsonKey(name: "stargazers_count") int? stargazersCount,
      @HiveField(15) @JsonKey(name: "watchers_count") int? watchersCount,
      @HiveField(16) @JsonKey(name: "language") String? language,
      @HiveField(17) @JsonKey(name: "has_issues") bool? hasIssues,
      @HiveField(18) @JsonKey(name: "has_projects") bool? hasProjects,
      @HiveField(19) @JsonKey(name: "has_downloads") bool? hasDownloads,
      @HiveField(20) @JsonKey(name: "has_wiki") bool? hasWiki,
      @HiveField(21) @JsonKey(name: "has_pages") bool? hasPages,
      @HiveField(22) @JsonKey(name: "has_discussions") bool? hasDiscussions,
      @HiveField(23) @JsonKey(name: "forks_count") int? forksCount,
      @HiveField(24) @JsonKey(name: "open_issues_count") int? openIssuesCount,
      @HiveField(25) @JsonKey(name: "license") License? license,
      @HiveField(26) @JsonKey(name: "allow_forking") bool? allowForking,
      @HiveField(27) @JsonKey(name: "topics") List<String>? topics,
      @HiveField(28) @JsonKey(name: "visibility") String? visibility,
      @HiveField(29) @JsonKey(name: "forks") int? forks,
      @HiveField(30) @JsonKey(name: "open_issues") int? openIssues,
      @HiveField(31) @JsonKey(name: "watchers") int? watchers,
      @HiveField(32) @JsonKey(name: "default_branch") String? defaultBranch,
      @HiveField(33) @JsonKey(name: "score") double? score});

  @override
  $OwnerCopyWith<$Res>? get owner;
  @override
  $LicenseCopyWith<$Res>? get license;
}

/// @nodoc
class __$$RepositoryImplCopyWithImpl<$Res>
    extends _$RepositoryCopyWithImpl<$Res, _$RepositoryImpl>
    implements _$$RepositoryImplCopyWith<$Res> {
  __$$RepositoryImplCopyWithImpl(
      _$RepositoryImpl _value, $Res Function(_$RepositoryImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? nodeId = freezed,
    Object? name = freezed,
    Object? fullName = freezed,
    Object? private = freezed,
    Object? owner = freezed,
    Object? htmlUrl = freezed,
    Object? description = freezed,
    Object? fork = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? pushedAt = freezed,
    Object? size = freezed,
    Object? stargazersCount = freezed,
    Object? watchersCount = freezed,
    Object? language = freezed,
    Object? hasIssues = freezed,
    Object? hasProjects = freezed,
    Object? hasDownloads = freezed,
    Object? hasWiki = freezed,
    Object? hasPages = freezed,
    Object? hasDiscussions = freezed,
    Object? forksCount = freezed,
    Object? openIssuesCount = freezed,
    Object? license = freezed,
    Object? allowForking = freezed,
    Object? topics = freezed,
    Object? visibility = freezed,
    Object? forks = freezed,
    Object? openIssues = freezed,
    Object? watchers = freezed,
    Object? defaultBranch = freezed,
    Object? score = freezed,
  }) {
    return _then(_$RepositoryImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nodeId: freezed == nodeId
          ? _value.nodeId
          : nodeId // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      fullName: freezed == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      private: freezed == private
          ? _value.private
          : private // ignore: cast_nullable_to_non_nullable
              as bool?,
      owner: freezed == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Owner?,
      htmlUrl: freezed == htmlUrl
          ? _value.htmlUrl
          : htmlUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      fork: freezed == fork
          ? _value.fork
          : fork // ignore: cast_nullable_to_non_nullable
              as bool?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      pushedAt: freezed == pushedAt
          ? _value.pushedAt
          : pushedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      size: freezed == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int?,
      stargazersCount: freezed == stargazersCount
          ? _value.stargazersCount
          : stargazersCount // ignore: cast_nullable_to_non_nullable
              as int?,
      watchersCount: freezed == watchersCount
          ? _value.watchersCount
          : watchersCount // ignore: cast_nullable_to_non_nullable
              as int?,
      language: freezed == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      hasIssues: freezed == hasIssues
          ? _value.hasIssues
          : hasIssues // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasProjects: freezed == hasProjects
          ? _value.hasProjects
          : hasProjects // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasDownloads: freezed == hasDownloads
          ? _value.hasDownloads
          : hasDownloads // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasWiki: freezed == hasWiki
          ? _value.hasWiki
          : hasWiki // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasPages: freezed == hasPages
          ? _value.hasPages
          : hasPages // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasDiscussions: freezed == hasDiscussions
          ? _value.hasDiscussions
          : hasDiscussions // ignore: cast_nullable_to_non_nullable
              as bool?,
      forksCount: freezed == forksCount
          ? _value.forksCount
          : forksCount // ignore: cast_nullable_to_non_nullable
              as int?,
      openIssuesCount: freezed == openIssuesCount
          ? _value.openIssuesCount
          : openIssuesCount // ignore: cast_nullable_to_non_nullable
              as int?,
      license: freezed == license
          ? _value.license
          : license // ignore: cast_nullable_to_non_nullable
              as License?,
      allowForking: freezed == allowForking
          ? _value.allowForking
          : allowForking // ignore: cast_nullable_to_non_nullable
              as bool?,
      topics: freezed == topics
          ? _value._topics
          : topics // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      visibility: freezed == visibility
          ? _value.visibility
          : visibility // ignore: cast_nullable_to_non_nullable
              as String?,
      forks: freezed == forks
          ? _value.forks
          : forks // ignore: cast_nullable_to_non_nullable
              as int?,
      openIssues: freezed == openIssues
          ? _value.openIssues
          : openIssues // ignore: cast_nullable_to_non_nullable
              as int?,
      watchers: freezed == watchers
          ? _value.watchers
          : watchers // ignore: cast_nullable_to_non_nullable
              as int?,
      defaultBranch: freezed == defaultBranch
          ? _value.defaultBranch
          : defaultBranch // ignore: cast_nullable_to_non_nullable
              as String?,
      score: freezed == score
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RepositoryImpl implements _Repository {
  const _$RepositoryImpl(
      {@HiveField(0) @JsonKey(name: "id") this.id,
      @HiveField(1) @JsonKey(name: "node_id") this.nodeId,
      @HiveField(3) @JsonKey(name: "name") this.name,
      @HiveField(4) @JsonKey(name: "full_name") this.fullName,
      @HiveField(5) @JsonKey(name: "private") this.private,
      @HiveField(6) @JsonKey(name: "owner") this.owner,
      @HiveField(7) @JsonKey(name: "html_url") this.htmlUrl,
      @HiveField(8) @JsonKey(name: "description") this.description,
      @HiveField(9) @JsonKey(name: "fork") this.fork,
      @HiveField(10) @JsonKey(name: "created_at") this.createdAt,
      @HiveField(11) @JsonKey(name: "updated_at") this.updatedAt,
      @HiveField(12) @JsonKey(name: "pushed_at") this.pushedAt,
      @HiveField(13) @JsonKey(name: "size") this.size,
      @HiveField(14) @JsonKey(name: "stargazers_count") this.stargazersCount,
      @HiveField(15) @JsonKey(name: "watchers_count") this.watchersCount,
      @HiveField(16) @JsonKey(name: "language") this.language,
      @HiveField(17) @JsonKey(name: "has_issues") this.hasIssues,
      @HiveField(18) @JsonKey(name: "has_projects") this.hasProjects,
      @HiveField(19) @JsonKey(name: "has_downloads") this.hasDownloads,
      @HiveField(20) @JsonKey(name: "has_wiki") this.hasWiki,
      @HiveField(21) @JsonKey(name: "has_pages") this.hasPages,
      @HiveField(22) @JsonKey(name: "has_discussions") this.hasDiscussions,
      @HiveField(23) @JsonKey(name: "forks_count") this.forksCount,
      @HiveField(24) @JsonKey(name: "open_issues_count") this.openIssuesCount,
      @HiveField(25) @JsonKey(name: "license") this.license,
      @HiveField(26) @JsonKey(name: "allow_forking") this.allowForking,
      @HiveField(27) @JsonKey(name: "topics") final List<String>? topics,
      @HiveField(28) @JsonKey(name: "visibility") this.visibility,
      @HiveField(29) @JsonKey(name: "forks") this.forks,
      @HiveField(30) @JsonKey(name: "open_issues") this.openIssues,
      @HiveField(31) @JsonKey(name: "watchers") this.watchers,
      @HiveField(32) @JsonKey(name: "default_branch") this.defaultBranch,
      @HiveField(33) @JsonKey(name: "score") this.score})
      : _topics = topics;

  factory _$RepositoryImpl.fromJson(Map<String, dynamic> json) =>
      _$$RepositoryImplFromJson(json);

  @override
  @HiveField(0)
  @JsonKey(name: "id")
  final int? id;
  @override
  @HiveField(1)
  @JsonKey(name: "node_id")
  final String? nodeId;
  @override
  @HiveField(3)
  @JsonKey(name: "name")
  final String? name;
  @override
  @HiveField(4)
  @JsonKey(name: "full_name")
  final String? fullName;
  @override
  @HiveField(5)
  @JsonKey(name: "private")
  final bool? private;
  @override
  @HiveField(6)
  @JsonKey(name: "owner")
  final Owner? owner;
  @override
  @HiveField(7)
  @JsonKey(name: "html_url")
  final String? htmlUrl;
  @override
  @HiveField(8)
  @JsonKey(name: "description")
  final String? description;
  @override
  @HiveField(9)
  @JsonKey(name: "fork")
  final bool? fork;
  @override
  @HiveField(10)
  @JsonKey(name: "created_at")
  final DateTime? createdAt;
  @override
  @HiveField(11)
  @JsonKey(name: "updated_at")
  final DateTime? updatedAt;
  @override
  @HiveField(12)
  @JsonKey(name: "pushed_at")
  final DateTime? pushedAt;
  @override
  @HiveField(13)
  @JsonKey(name: "size")
  final int? size;
  @override
  @HiveField(14)
  @JsonKey(name: "stargazers_count")
  final int? stargazersCount;
  @override
  @HiveField(15)
  @JsonKey(name: "watchers_count")
  final int? watchersCount;
  @override
  @HiveField(16)
  @JsonKey(name: "language")
  final String? language;
  @override
  @HiveField(17)
  @JsonKey(name: "has_issues")
  final bool? hasIssues;
  @override
  @HiveField(18)
  @JsonKey(name: "has_projects")
  final bool? hasProjects;
  @override
  @HiveField(19)
  @JsonKey(name: "has_downloads")
  final bool? hasDownloads;
  @override
  @HiveField(20)
  @JsonKey(name: "has_wiki")
  final bool? hasWiki;
  @override
  @HiveField(21)
  @JsonKey(name: "has_pages")
  final bool? hasPages;
  @override
  @HiveField(22)
  @JsonKey(name: "has_discussions")
  final bool? hasDiscussions;
  @override
  @HiveField(23)
  @JsonKey(name: "forks_count")
  final int? forksCount;
  @override
  @HiveField(24)
  @JsonKey(name: "open_issues_count")
  final int? openIssuesCount;
  @override
  @HiveField(25)
  @JsonKey(name: "license")
  final License? license;
  @override
  @HiveField(26)
  @JsonKey(name: "allow_forking")
  final bool? allowForking;
  final List<String>? _topics;
  @override
  @HiveField(27)
  @JsonKey(name: "topics")
  List<String>? get topics {
    final value = _topics;
    if (value == null) return null;
    if (_topics is EqualUnmodifiableListView) return _topics;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @HiveField(28)
  @JsonKey(name: "visibility")
  final String? visibility;
  @override
  @HiveField(29)
  @JsonKey(name: "forks")
  final int? forks;
  @override
  @HiveField(30)
  @JsonKey(name: "open_issues")
  final int? openIssues;
  @override
  @HiveField(31)
  @JsonKey(name: "watchers")
  final int? watchers;
  @override
  @HiveField(32)
  @JsonKey(name: "default_branch")
  final String? defaultBranch;
  @override
  @HiveField(33)
  @JsonKey(name: "score")
  final double? score;

  @override
  String toString() {
    return 'Repository(id: $id, nodeId: $nodeId, name: $name, fullName: $fullName, private: $private, owner: $owner, htmlUrl: $htmlUrl, description: $description, fork: $fork, createdAt: $createdAt, updatedAt: $updatedAt, pushedAt: $pushedAt, size: $size, stargazersCount: $stargazersCount, watchersCount: $watchersCount, language: $language, hasIssues: $hasIssues, hasProjects: $hasProjects, hasDownloads: $hasDownloads, hasWiki: $hasWiki, hasPages: $hasPages, hasDiscussions: $hasDiscussions, forksCount: $forksCount, openIssuesCount: $openIssuesCount, license: $license, allowForking: $allowForking, topics: $topics, visibility: $visibility, forks: $forks, openIssues: $openIssues, watchers: $watchers, defaultBranch: $defaultBranch, score: $score)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RepositoryImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.nodeId, nodeId) || other.nodeId == nodeId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.private, private) || other.private == private) &&
            (identical(other.owner, owner) || other.owner == owner) &&
            (identical(other.htmlUrl, htmlUrl) || other.htmlUrl == htmlUrl) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.fork, fork) || other.fork == fork) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.pushedAt, pushedAt) ||
                other.pushedAt == pushedAt) &&
            (identical(other.size, size) || other.size == size) &&
            (identical(other.stargazersCount, stargazersCount) ||
                other.stargazersCount == stargazersCount) &&
            (identical(other.watchersCount, watchersCount) ||
                other.watchersCount == watchersCount) &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.hasIssues, hasIssues) ||
                other.hasIssues == hasIssues) &&
            (identical(other.hasProjects, hasProjects) ||
                other.hasProjects == hasProjects) &&
            (identical(other.hasDownloads, hasDownloads) ||
                other.hasDownloads == hasDownloads) &&
            (identical(other.hasWiki, hasWiki) || other.hasWiki == hasWiki) &&
            (identical(other.hasPages, hasPages) ||
                other.hasPages == hasPages) &&
            (identical(other.hasDiscussions, hasDiscussions) ||
                other.hasDiscussions == hasDiscussions) &&
            (identical(other.forksCount, forksCount) ||
                other.forksCount == forksCount) &&
            (identical(other.openIssuesCount, openIssuesCount) ||
                other.openIssuesCount == openIssuesCount) &&
            (identical(other.license, license) || other.license == license) &&
            (identical(other.allowForking, allowForking) ||
                other.allowForking == allowForking) &&
            const DeepCollectionEquality().equals(other._topics, _topics) &&
            (identical(other.visibility, visibility) ||
                other.visibility == visibility) &&
            (identical(other.forks, forks) || other.forks == forks) &&
            (identical(other.openIssues, openIssues) ||
                other.openIssues == openIssues) &&
            (identical(other.watchers, watchers) ||
                other.watchers == watchers) &&
            (identical(other.defaultBranch, defaultBranch) ||
                other.defaultBranch == defaultBranch) &&
            (identical(other.score, score) || other.score == score));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        nodeId,
        name,
        fullName,
        private,
        owner,
        htmlUrl,
        description,
        fork,
        createdAt,
        updatedAt,
        pushedAt,
        size,
        stargazersCount,
        watchersCount,
        language,
        hasIssues,
        hasProjects,
        hasDownloads,
        hasWiki,
        hasPages,
        hasDiscussions,
        forksCount,
        openIssuesCount,
        license,
        allowForking,
        const DeepCollectionEquality().hash(_topics),
        visibility,
        forks,
        openIssues,
        watchers,
        defaultBranch,
        score
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RepositoryImplCopyWith<_$RepositoryImpl> get copyWith =>
      __$$RepositoryImplCopyWithImpl<_$RepositoryImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RepositoryImplToJson(
      this,
    );
  }
}

abstract class _Repository implements Repository {
  const factory _Repository(
      {@HiveField(0) @JsonKey(name: "id") final int? id,
      @HiveField(1) @JsonKey(name: "node_id") final String? nodeId,
      @HiveField(3) @JsonKey(name: "name") final String? name,
      @HiveField(4) @JsonKey(name: "full_name") final String? fullName,
      @HiveField(5) @JsonKey(name: "private") final bool? private,
      @HiveField(6) @JsonKey(name: "owner") final Owner? owner,
      @HiveField(7) @JsonKey(name: "html_url") final String? htmlUrl,
      @HiveField(8) @JsonKey(name: "description") final String? description,
      @HiveField(9) @JsonKey(name: "fork") final bool? fork,
      @HiveField(10) @JsonKey(name: "created_at") final DateTime? createdAt,
      @HiveField(11) @JsonKey(name: "updated_at") final DateTime? updatedAt,
      @HiveField(12) @JsonKey(name: "pushed_at") final DateTime? pushedAt,
      @HiveField(13) @JsonKey(name: "size") final int? size,
      @HiveField(14)
      @JsonKey(name: "stargazers_count")
      final int? stargazersCount,
      @HiveField(15) @JsonKey(name: "watchers_count") final int? watchersCount,
      @HiveField(16) @JsonKey(name: "language") final String? language,
      @HiveField(17) @JsonKey(name: "has_issues") final bool? hasIssues,
      @HiveField(18) @JsonKey(name: "has_projects") final bool? hasProjects,
      @HiveField(19) @JsonKey(name: "has_downloads") final bool? hasDownloads,
      @HiveField(20) @JsonKey(name: "has_wiki") final bool? hasWiki,
      @HiveField(21) @JsonKey(name: "has_pages") final bool? hasPages,
      @HiveField(22)
      @JsonKey(name: "has_discussions")
      final bool? hasDiscussions,
      @HiveField(23) @JsonKey(name: "forks_count") final int? forksCount,
      @HiveField(24)
      @JsonKey(name: "open_issues_count")
      final int? openIssuesCount,
      @HiveField(25) @JsonKey(name: "license") final License? license,
      @HiveField(26) @JsonKey(name: "allow_forking") final bool? allowForking,
      @HiveField(27) @JsonKey(name: "topics") final List<String>? topics,
      @HiveField(28) @JsonKey(name: "visibility") final String? visibility,
      @HiveField(29) @JsonKey(name: "forks") final int? forks,
      @HiveField(30) @JsonKey(name: "open_issues") final int? openIssues,
      @HiveField(31) @JsonKey(name: "watchers") final int? watchers,
      @HiveField(32)
      @JsonKey(name: "default_branch")
      final String? defaultBranch,
      @HiveField(33)
      @JsonKey(name: "score")
      final double? score}) = _$RepositoryImpl;

  factory _Repository.fromJson(Map<String, dynamic> json) =
      _$RepositoryImpl.fromJson;

  @override
  @HiveField(0)
  @JsonKey(name: "id")
  int? get id;
  @override
  @HiveField(1)
  @JsonKey(name: "node_id")
  String? get nodeId;
  @override
  @HiveField(3)
  @JsonKey(name: "name")
  String? get name;
  @override
  @HiveField(4)
  @JsonKey(name: "full_name")
  String? get fullName;
  @override
  @HiveField(5)
  @JsonKey(name: "private")
  bool? get private;
  @override
  @HiveField(6)
  @JsonKey(name: "owner")
  Owner? get owner;
  @override
  @HiveField(7)
  @JsonKey(name: "html_url")
  String? get htmlUrl;
  @override
  @HiveField(8)
  @JsonKey(name: "description")
  String? get description;
  @override
  @HiveField(9)
  @JsonKey(name: "fork")
  bool? get fork;
  @override
  @HiveField(10)
  @JsonKey(name: "created_at")
  DateTime? get createdAt;
  @override
  @HiveField(11)
  @JsonKey(name: "updated_at")
  DateTime? get updatedAt;
  @override
  @HiveField(12)
  @JsonKey(name: "pushed_at")
  DateTime? get pushedAt;
  @override
  @HiveField(13)
  @JsonKey(name: "size")
  int? get size;
  @override
  @HiveField(14)
  @JsonKey(name: "stargazers_count")
  int? get stargazersCount;
  @override
  @HiveField(15)
  @JsonKey(name: "watchers_count")
  int? get watchersCount;
  @override
  @HiveField(16)
  @JsonKey(name: "language")
  String? get language;
  @override
  @HiveField(17)
  @JsonKey(name: "has_issues")
  bool? get hasIssues;
  @override
  @HiveField(18)
  @JsonKey(name: "has_projects")
  bool? get hasProjects;
  @override
  @HiveField(19)
  @JsonKey(name: "has_downloads")
  bool? get hasDownloads;
  @override
  @HiveField(20)
  @JsonKey(name: "has_wiki")
  bool? get hasWiki;
  @override
  @HiveField(21)
  @JsonKey(name: "has_pages")
  bool? get hasPages;
  @override
  @HiveField(22)
  @JsonKey(name: "has_discussions")
  bool? get hasDiscussions;
  @override
  @HiveField(23)
  @JsonKey(name: "forks_count")
  int? get forksCount;
  @override
  @HiveField(24)
  @JsonKey(name: "open_issues_count")
  int? get openIssuesCount;
  @override
  @HiveField(25)
  @JsonKey(name: "license")
  License? get license;
  @override
  @HiveField(26)
  @JsonKey(name: "allow_forking")
  bool? get allowForking;
  @override
  @HiveField(27)
  @JsonKey(name: "topics")
  List<String>? get topics;
  @override
  @HiveField(28)
  @JsonKey(name: "visibility")
  String? get visibility;
  @override
  @HiveField(29)
  @JsonKey(name: "forks")
  int? get forks;
  @override
  @HiveField(30)
  @JsonKey(name: "open_issues")
  int? get openIssues;
  @override
  @HiveField(31)
  @JsonKey(name: "watchers")
  int? get watchers;
  @override
  @HiveField(32)
  @JsonKey(name: "default_branch")
  String? get defaultBranch;
  @override
  @HiveField(33)
  @JsonKey(name: "score")
  double? get score;
  @override
  @JsonKey(ignore: true)
  _$$RepositoryImplCopyWith<_$RepositoryImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

License _$LicenseFromJson(Map<String, dynamic> json) {
  return _License.fromJson(json);
}

/// @nodoc
mixin _$License {
  @HiveField(0)
  @JsonKey(name: "key")
  String? get key => throw _privateConstructorUsedError;
  @HiveField(1)
  @JsonKey(name: "name")
  String? get name => throw _privateConstructorUsedError;
  @HiveField(2)
  @JsonKey(name: "spdx_id")
  String? get spdxId => throw _privateConstructorUsedError;
  @HiveField(3)
  @JsonKey(name: "url")
  String? get url => throw _privateConstructorUsedError;
  @HiveField(4)
  @JsonKey(name: "node_id")
  String? get nodeId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LicenseCopyWith<License> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LicenseCopyWith<$Res> {
  factory $LicenseCopyWith(License value, $Res Function(License) then) =
      _$LicenseCopyWithImpl<$Res, License>;
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "key") String? key,
      @HiveField(1) @JsonKey(name: "name") String? name,
      @HiveField(2) @JsonKey(name: "spdx_id") String? spdxId,
      @HiveField(3) @JsonKey(name: "url") String? url,
      @HiveField(4) @JsonKey(name: "node_id") String? nodeId});
}

/// @nodoc
class _$LicenseCopyWithImpl<$Res, $Val extends License>
    implements $LicenseCopyWith<$Res> {
  _$LicenseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? key = freezed,
    Object? name = freezed,
    Object? spdxId = freezed,
    Object? url = freezed,
    Object? nodeId = freezed,
  }) {
    return _then(_value.copyWith(
      key: freezed == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      spdxId: freezed == spdxId
          ? _value.spdxId
          : spdxId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      nodeId: freezed == nodeId
          ? _value.nodeId
          : nodeId // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LicenseImplCopyWith<$Res> implements $LicenseCopyWith<$Res> {
  factory _$$LicenseImplCopyWith(
          _$LicenseImpl value, $Res Function(_$LicenseImpl) then) =
      __$$LicenseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "key") String? key,
      @HiveField(1) @JsonKey(name: "name") String? name,
      @HiveField(2) @JsonKey(name: "spdx_id") String? spdxId,
      @HiveField(3) @JsonKey(name: "url") String? url,
      @HiveField(4) @JsonKey(name: "node_id") String? nodeId});
}

/// @nodoc
class __$$LicenseImplCopyWithImpl<$Res>
    extends _$LicenseCopyWithImpl<$Res, _$LicenseImpl>
    implements _$$LicenseImplCopyWith<$Res> {
  __$$LicenseImplCopyWithImpl(
      _$LicenseImpl _value, $Res Function(_$LicenseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? key = freezed,
    Object? name = freezed,
    Object? spdxId = freezed,
    Object? url = freezed,
    Object? nodeId = freezed,
  }) {
    return _then(_$LicenseImpl(
      key: freezed == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      spdxId: freezed == spdxId
          ? _value.spdxId
          : spdxId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      nodeId: freezed == nodeId
          ? _value.nodeId
          : nodeId // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$LicenseImpl implements _License {
  const _$LicenseImpl(
      {@HiveField(0) @JsonKey(name: "key") this.key,
      @HiveField(1) @JsonKey(name: "name") this.name,
      @HiveField(2) @JsonKey(name: "spdx_id") this.spdxId,
      @HiveField(3) @JsonKey(name: "url") this.url,
      @HiveField(4) @JsonKey(name: "node_id") this.nodeId});

  factory _$LicenseImpl.fromJson(Map<String, dynamic> json) =>
      _$$LicenseImplFromJson(json);

  @override
  @HiveField(0)
  @JsonKey(name: "key")
  final String? key;
  @override
  @HiveField(1)
  @JsonKey(name: "name")
  final String? name;
  @override
  @HiveField(2)
  @JsonKey(name: "spdx_id")
  final String? spdxId;
  @override
  @HiveField(3)
  @JsonKey(name: "url")
  final String? url;
  @override
  @HiveField(4)
  @JsonKey(name: "node_id")
  final String? nodeId;

  @override
  String toString() {
    return 'License(key: $key, name: $name, spdxId: $spdxId, url: $url, nodeId: $nodeId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LicenseImpl &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.spdxId, spdxId) || other.spdxId == spdxId) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.nodeId, nodeId) || other.nodeId == nodeId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, key, name, spdxId, url, nodeId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LicenseImplCopyWith<_$LicenseImpl> get copyWith =>
      __$$LicenseImplCopyWithImpl<_$LicenseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$LicenseImplToJson(
      this,
    );
  }
}

abstract class _License implements License {
  const factory _License(
          {@HiveField(0) @JsonKey(name: "key") final String? key,
          @HiveField(1) @JsonKey(name: "name") final String? name,
          @HiveField(2) @JsonKey(name: "spdx_id") final String? spdxId,
          @HiveField(3) @JsonKey(name: "url") final String? url,
          @HiveField(4) @JsonKey(name: "node_id") final String? nodeId}) =
      _$LicenseImpl;

  factory _License.fromJson(Map<String, dynamic> json) = _$LicenseImpl.fromJson;

  @override
  @HiveField(0)
  @JsonKey(name: "key")
  String? get key;
  @override
  @HiveField(1)
  @JsonKey(name: "name")
  String? get name;
  @override
  @HiveField(2)
  @JsonKey(name: "spdx_id")
  String? get spdxId;
  @override
  @HiveField(3)
  @JsonKey(name: "url")
  String? get url;
  @override
  @HiveField(4)
  @JsonKey(name: "node_id")
  String? get nodeId;
  @override
  @JsonKey(ignore: true)
  _$$LicenseImplCopyWith<_$LicenseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

Owner _$OwnerFromJson(Map<String, dynamic> json) {
  return _Owner.fromJson(json);
}

/// @nodoc
mixin _$Owner {
  @HiveField(0)
  @JsonKey(name: "login")
  String? get login => throw _privateConstructorUsedError;
  @HiveField(1)
  @JsonKey(name: "id")
  int? get id => throw _privateConstructorUsedError;
  @HiveField(2)
  @JsonKey(name: "node_id")
  String? get nodeId => throw _privateConstructorUsedError;
  @HiveField(3)
  @JsonKey(name: "avatar_url")
  String? get avatarUrl => throw _privateConstructorUsedError;
  @HiveField(4)
  @JsonKey(name: "gravatar_id")
  String? get gravatarId => throw _privateConstructorUsedError;
  @HiveField(5)
  @JsonKey(name: "url")
  String? get url => throw _privateConstructorUsedError;
  @HiveField(6)
  @JsonKey(name: "html_url")
  String? get htmlUrl => throw _privateConstructorUsedError;
  @HiveField(7)
  @JsonKey(name: "type")
  String? get type => throw _privateConstructorUsedError;
  @HiveField(8)
  @JsonKey(name: "site_admin")
  bool? get siteAdmin => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $OwnerCopyWith<Owner> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OwnerCopyWith<$Res> {
  factory $OwnerCopyWith(Owner value, $Res Function(Owner) then) =
      _$OwnerCopyWithImpl<$Res, Owner>;
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "login") String? login,
      @HiveField(1) @JsonKey(name: "id") int? id,
      @HiveField(2) @JsonKey(name: "node_id") String? nodeId,
      @HiveField(3) @JsonKey(name: "avatar_url") String? avatarUrl,
      @HiveField(4) @JsonKey(name: "gravatar_id") String? gravatarId,
      @HiveField(5) @JsonKey(name: "url") String? url,
      @HiveField(6) @JsonKey(name: "html_url") String? htmlUrl,
      @HiveField(7) @JsonKey(name: "type") String? type,
      @HiveField(8) @JsonKey(name: "site_admin") bool? siteAdmin});
}

/// @nodoc
class _$OwnerCopyWithImpl<$Res, $Val extends Owner>
    implements $OwnerCopyWith<$Res> {
  _$OwnerCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? login = freezed,
    Object? id = freezed,
    Object? nodeId = freezed,
    Object? avatarUrl = freezed,
    Object? gravatarId = freezed,
    Object? url = freezed,
    Object? htmlUrl = freezed,
    Object? type = freezed,
    Object? siteAdmin = freezed,
  }) {
    return _then(_value.copyWith(
      login: freezed == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nodeId: freezed == nodeId
          ? _value.nodeId
          : nodeId // ignore: cast_nullable_to_non_nullable
              as String?,
      avatarUrl: freezed == avatarUrl
          ? _value.avatarUrl
          : avatarUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      gravatarId: freezed == gravatarId
          ? _value.gravatarId
          : gravatarId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      htmlUrl: freezed == htmlUrl
          ? _value.htmlUrl
          : htmlUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      siteAdmin: freezed == siteAdmin
          ? _value.siteAdmin
          : siteAdmin // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OwnerImplCopyWith<$Res> implements $OwnerCopyWith<$Res> {
  factory _$$OwnerImplCopyWith(
          _$OwnerImpl value, $Res Function(_$OwnerImpl) then) =
      __$$OwnerImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) @JsonKey(name: "login") String? login,
      @HiveField(1) @JsonKey(name: "id") int? id,
      @HiveField(2) @JsonKey(name: "node_id") String? nodeId,
      @HiveField(3) @JsonKey(name: "avatar_url") String? avatarUrl,
      @HiveField(4) @JsonKey(name: "gravatar_id") String? gravatarId,
      @HiveField(5) @JsonKey(name: "url") String? url,
      @HiveField(6) @JsonKey(name: "html_url") String? htmlUrl,
      @HiveField(7) @JsonKey(name: "type") String? type,
      @HiveField(8) @JsonKey(name: "site_admin") bool? siteAdmin});
}

/// @nodoc
class __$$OwnerImplCopyWithImpl<$Res>
    extends _$OwnerCopyWithImpl<$Res, _$OwnerImpl>
    implements _$$OwnerImplCopyWith<$Res> {
  __$$OwnerImplCopyWithImpl(
      _$OwnerImpl _value, $Res Function(_$OwnerImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? login = freezed,
    Object? id = freezed,
    Object? nodeId = freezed,
    Object? avatarUrl = freezed,
    Object? gravatarId = freezed,
    Object? url = freezed,
    Object? htmlUrl = freezed,
    Object? type = freezed,
    Object? siteAdmin = freezed,
  }) {
    return _then(_$OwnerImpl(
      login: freezed == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      nodeId: freezed == nodeId
          ? _value.nodeId
          : nodeId // ignore: cast_nullable_to_non_nullable
              as String?,
      avatarUrl: freezed == avatarUrl
          ? _value.avatarUrl
          : avatarUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      gravatarId: freezed == gravatarId
          ? _value.gravatarId
          : gravatarId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      htmlUrl: freezed == htmlUrl
          ? _value.htmlUrl
          : htmlUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      siteAdmin: freezed == siteAdmin
          ? _value.siteAdmin
          : siteAdmin // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$OwnerImpl implements _Owner {
  const _$OwnerImpl(
      {@HiveField(0) @JsonKey(name: "login") this.login,
      @HiveField(1) @JsonKey(name: "id") this.id,
      @HiveField(2) @JsonKey(name: "node_id") this.nodeId,
      @HiveField(3) @JsonKey(name: "avatar_url") this.avatarUrl,
      @HiveField(4) @JsonKey(name: "gravatar_id") this.gravatarId,
      @HiveField(5) @JsonKey(name: "url") this.url,
      @HiveField(6) @JsonKey(name: "html_url") this.htmlUrl,
      @HiveField(7) @JsonKey(name: "type") this.type,
      @HiveField(8) @JsonKey(name: "site_admin") this.siteAdmin});

  factory _$OwnerImpl.fromJson(Map<String, dynamic> json) =>
      _$$OwnerImplFromJson(json);

  @override
  @HiveField(0)
  @JsonKey(name: "login")
  final String? login;
  @override
  @HiveField(1)
  @JsonKey(name: "id")
  final int? id;
  @override
  @HiveField(2)
  @JsonKey(name: "node_id")
  final String? nodeId;
  @override
  @HiveField(3)
  @JsonKey(name: "avatar_url")
  final String? avatarUrl;
  @override
  @HiveField(4)
  @JsonKey(name: "gravatar_id")
  final String? gravatarId;
  @override
  @HiveField(5)
  @JsonKey(name: "url")
  final String? url;
  @override
  @HiveField(6)
  @JsonKey(name: "html_url")
  final String? htmlUrl;
  @override
  @HiveField(7)
  @JsonKey(name: "type")
  final String? type;
  @override
  @HiveField(8)
  @JsonKey(name: "site_admin")
  final bool? siteAdmin;

  @override
  String toString() {
    return 'Owner(login: $login, id: $id, nodeId: $nodeId, avatarUrl: $avatarUrl, gravatarId: $gravatarId, url: $url, htmlUrl: $htmlUrl, type: $type, siteAdmin: $siteAdmin)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OwnerImpl &&
            (identical(other.login, login) || other.login == login) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.nodeId, nodeId) || other.nodeId == nodeId) &&
            (identical(other.avatarUrl, avatarUrl) ||
                other.avatarUrl == avatarUrl) &&
            (identical(other.gravatarId, gravatarId) ||
                other.gravatarId == gravatarId) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.htmlUrl, htmlUrl) || other.htmlUrl == htmlUrl) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.siteAdmin, siteAdmin) ||
                other.siteAdmin == siteAdmin));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, login, id, nodeId, avatarUrl,
      gravatarId, url, htmlUrl, type, siteAdmin);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OwnerImplCopyWith<_$OwnerImpl> get copyWith =>
      __$$OwnerImplCopyWithImpl<_$OwnerImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$OwnerImplToJson(
      this,
    );
  }
}

abstract class _Owner implements Owner {
  const factory _Owner(
          {@HiveField(0) @JsonKey(name: "login") final String? login,
          @HiveField(1) @JsonKey(name: "id") final int? id,
          @HiveField(2) @JsonKey(name: "node_id") final String? nodeId,
          @HiveField(3) @JsonKey(name: "avatar_url") final String? avatarUrl,
          @HiveField(4) @JsonKey(name: "gravatar_id") final String? gravatarId,
          @HiveField(5) @JsonKey(name: "url") final String? url,
          @HiveField(6) @JsonKey(name: "html_url") final String? htmlUrl,
          @HiveField(7) @JsonKey(name: "type") final String? type,
          @HiveField(8) @JsonKey(name: "site_admin") final bool? siteAdmin}) =
      _$OwnerImpl;

  factory _Owner.fromJson(Map<String, dynamic> json) = _$OwnerImpl.fromJson;

  @override
  @HiveField(0)
  @JsonKey(name: "login")
  String? get login;
  @override
  @HiveField(1)
  @JsonKey(name: "id")
  int? get id;
  @override
  @HiveField(2)
  @JsonKey(name: "node_id")
  String? get nodeId;
  @override
  @HiveField(3)
  @JsonKey(name: "avatar_url")
  String? get avatarUrl;
  @override
  @HiveField(4)
  @JsonKey(name: "gravatar_id")
  String? get gravatarId;
  @override
  @HiveField(5)
  @JsonKey(name: "url")
  String? get url;
  @override
  @HiveField(6)
  @JsonKey(name: "html_url")
  String? get htmlUrl;
  @override
  @HiveField(7)
  @JsonKey(name: "type")
  String? get type;
  @override
  @HiveField(8)
  @JsonKey(name: "site_admin")
  bool? get siteAdmin;
  @override
  @JsonKey(ignore: true)
  _$$OwnerImplCopyWith<_$OwnerImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
