// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repository_response.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RepositoryResponseAdapter extends TypeAdapter<RepositoryResponse> {
  @override
  final int typeId = 0;

  @override
  RepositoryResponse read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RepositoryResponse(
      totalCount: fields[0] as int?,
      incompleteResults: fields[1] as bool?,
      items: (fields[2] as List?)?.cast<Repository>(),
    );
  }

  @override
  void write(BinaryWriter writer, RepositoryResponse obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.totalCount)
      ..writeByte(1)
      ..write(obj.incompleteResults)
      ..writeByte(2)
      ..write(obj.items);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RepositoryResponseAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class RepositoryAdapter extends TypeAdapter<Repository> {
  @override
  final int typeId = 1;

  @override
  Repository read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Repository(
      id: fields[0] as int?,
      nodeId: fields[1] as String?,
      name: fields[3] as String?,
      fullName: fields[4] as String?,
      private: fields[5] as bool?,
      owner: fields[6] as Owner?,
      htmlUrl: fields[7] as String?,
      description: fields[8] as String?,
      fork: fields[9] as bool?,
      createdAt: fields[10] as DateTime?,
      updatedAt: fields[11] as DateTime?,
      pushedAt: fields[12] as DateTime?,
      size: fields[13] as int?,
      stargazersCount: fields[14] as int?,
      watchersCount: fields[15] as int?,
      language: fields[16] as String?,
      hasIssues: fields[17] as bool?,
      hasProjects: fields[18] as bool?,
      hasDownloads: fields[19] as bool?,
      hasWiki: fields[20] as bool?,
      hasPages: fields[21] as bool?,
      hasDiscussions: fields[22] as bool?,
      forksCount: fields[23] as int?,
      openIssuesCount: fields[24] as int?,
      license: fields[25] as License?,
      allowForking: fields[26] as bool?,
      topics: (fields[27] as List?)?.cast<String>(),
      visibility: fields[28] as String?,
      forks: fields[29] as int?,
      openIssues: fields[30] as int?,
      watchers: fields[31] as int?,
      defaultBranch: fields[32] as String?,
      score: fields[33] as double?,
    );
  }

  @override
  void write(BinaryWriter writer, Repository obj) {
    writer
      ..writeByte(33)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.nodeId)
      ..writeByte(3)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.fullName)
      ..writeByte(5)
      ..write(obj.private)
      ..writeByte(6)
      ..write(obj.owner)
      ..writeByte(7)
      ..write(obj.htmlUrl)
      ..writeByte(8)
      ..write(obj.description)
      ..writeByte(9)
      ..write(obj.fork)
      ..writeByte(10)
      ..write(obj.createdAt)
      ..writeByte(11)
      ..write(obj.updatedAt)
      ..writeByte(12)
      ..write(obj.pushedAt)
      ..writeByte(13)
      ..write(obj.size)
      ..writeByte(14)
      ..write(obj.stargazersCount)
      ..writeByte(15)
      ..write(obj.watchersCount)
      ..writeByte(16)
      ..write(obj.language)
      ..writeByte(17)
      ..write(obj.hasIssues)
      ..writeByte(18)
      ..write(obj.hasProjects)
      ..writeByte(19)
      ..write(obj.hasDownloads)
      ..writeByte(20)
      ..write(obj.hasWiki)
      ..writeByte(21)
      ..write(obj.hasPages)
      ..writeByte(22)
      ..write(obj.hasDiscussions)
      ..writeByte(23)
      ..write(obj.forksCount)
      ..writeByte(24)
      ..write(obj.openIssuesCount)
      ..writeByte(25)
      ..write(obj.license)
      ..writeByte(26)
      ..write(obj.allowForking)
      ..writeByte(27)
      ..write(obj.topics)
      ..writeByte(28)
      ..write(obj.visibility)
      ..writeByte(29)
      ..write(obj.forks)
      ..writeByte(30)
      ..write(obj.openIssues)
      ..writeByte(31)
      ..write(obj.watchers)
      ..writeByte(32)
      ..write(obj.defaultBranch)
      ..writeByte(33)
      ..write(obj.score);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RepositoryAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class LicenseAdapter extends TypeAdapter<License> {
  @override
  final int typeId = 2;

  @override
  License read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return License(
      key: fields[0] as String?,
      name: fields[1] as String?,
      spdxId: fields[2] as String?,
      url: fields[3] as String?,
      nodeId: fields[4] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, License obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.key)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.spdxId)
      ..writeByte(3)
      ..write(obj.url)
      ..writeByte(4)
      ..write(obj.nodeId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LicenseAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class OwnerAdapter extends TypeAdapter<Owner> {
  @override
  final int typeId = 3;

  @override
  Owner read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Owner(
      login: fields[0] as String?,
      id: fields[1] as int?,
      nodeId: fields[2] as String?,
      avatarUrl: fields[3] as String?,
      gravatarId: fields[4] as String?,
      url: fields[5] as String?,
      htmlUrl: fields[6] as String?,
      type: fields[7] as String?,
      siteAdmin: fields[8] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, Owner obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.login)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.nodeId)
      ..writeByte(3)
      ..write(obj.avatarUrl)
      ..writeByte(4)
      ..write(obj.gravatarId)
      ..writeByte(5)
      ..write(obj.url)
      ..writeByte(6)
      ..write(obj.htmlUrl)
      ..writeByte(7)
      ..write(obj.type)
      ..writeByte(8)
      ..write(obj.siteAdmin);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OwnerAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RepositoryResponseImpl _$$RepositoryResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$RepositoryResponseImpl(
      totalCount: json['total_count'] as int?,
      incompleteResults: json['incomplete_results'] as bool?,
      items: (json['items'] as List<dynamic>?)
          ?.map((e) => Repository.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$RepositoryResponseImplToJson(
        _$RepositoryResponseImpl instance) =>
    <String, dynamic>{
      'total_count': instance.totalCount,
      'incomplete_results': instance.incompleteResults,
      'items': instance.items,
    };

_$RepositoryImpl _$$RepositoryImplFromJson(Map<String, dynamic> json) =>
    _$RepositoryImpl(
      id: json['id'] as int?,
      nodeId: json['node_id'] as String?,
      name: json['name'] as String?,
      fullName: json['full_name'] as String?,
      private: json['private'] as bool?,
      owner: json['owner'] == null
          ? null
          : Owner.fromJson(json['owner'] as Map<String, dynamic>),
      htmlUrl: json['html_url'] as String?,
      description: json['description'] as String?,
      fork: json['fork'] as bool?,
      createdAt: json['created_at'] == null
          ? null
          : DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
      pushedAt: json['pushed_at'] == null
          ? null
          : DateTime.parse(json['pushed_at'] as String),
      size: json['size'] as int?,
      stargazersCount: json['stargazers_count'] as int?,
      watchersCount: json['watchers_count'] as int?,
      language: json['language'] as String?,
      hasIssues: json['has_issues'] as bool?,
      hasProjects: json['has_projects'] as bool?,
      hasDownloads: json['has_downloads'] as bool?,
      hasWiki: json['has_wiki'] as bool?,
      hasPages: json['has_pages'] as bool?,
      hasDiscussions: json['has_discussions'] as bool?,
      forksCount: json['forks_count'] as int?,
      openIssuesCount: json['open_issues_count'] as int?,
      license: json['license'] == null
          ? null
          : License.fromJson(json['license'] as Map<String, dynamic>),
      allowForking: json['allow_forking'] as bool?,
      topics:
          (json['topics'] as List<dynamic>?)?.map((e) => e as String).toList(),
      visibility: json['visibility'] as String?,
      forks: json['forks'] as int?,
      openIssues: json['open_issues'] as int?,
      watchers: json['watchers'] as int?,
      defaultBranch: json['default_branch'] as String?,
      score: (json['score'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$RepositoryImplToJson(_$RepositoryImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'node_id': instance.nodeId,
      'name': instance.name,
      'full_name': instance.fullName,
      'private': instance.private,
      'owner': instance.owner,
      'html_url': instance.htmlUrl,
      'description': instance.description,
      'fork': instance.fork,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'pushed_at': instance.pushedAt?.toIso8601String(),
      'size': instance.size,
      'stargazers_count': instance.stargazersCount,
      'watchers_count': instance.watchersCount,
      'language': instance.language,
      'has_issues': instance.hasIssues,
      'has_projects': instance.hasProjects,
      'has_downloads': instance.hasDownloads,
      'has_wiki': instance.hasWiki,
      'has_pages': instance.hasPages,
      'has_discussions': instance.hasDiscussions,
      'forks_count': instance.forksCount,
      'open_issues_count': instance.openIssuesCount,
      'license': instance.license,
      'allow_forking': instance.allowForking,
      'topics': instance.topics,
      'visibility': instance.visibility,
      'forks': instance.forks,
      'open_issues': instance.openIssues,
      'watchers': instance.watchers,
      'default_branch': instance.defaultBranch,
      'score': instance.score,
    };

_$LicenseImpl _$$LicenseImplFromJson(Map<String, dynamic> json) =>
    _$LicenseImpl(
      key: json['key'] as String?,
      name: json['name'] as String?,
      spdxId: json['spdx_id'] as String?,
      url: json['url'] as String?,
      nodeId: json['node_id'] as String?,
    );

Map<String, dynamic> _$$LicenseImplToJson(_$LicenseImpl instance) =>
    <String, dynamic>{
      'key': instance.key,
      'name': instance.name,
      'spdx_id': instance.spdxId,
      'url': instance.url,
      'node_id': instance.nodeId,
    };

_$OwnerImpl _$$OwnerImplFromJson(Map<String, dynamic> json) => _$OwnerImpl(
      login: json['login'] as String?,
      id: json['id'] as int?,
      nodeId: json['node_id'] as String?,
      avatarUrl: json['avatar_url'] as String?,
      gravatarId: json['gravatar_id'] as String?,
      url: json['url'] as String?,
      htmlUrl: json['html_url'] as String?,
      type: json['type'] as String?,
      siteAdmin: json['site_admin'] as bool?,
    );

Map<String, dynamic> _$$OwnerImplToJson(_$OwnerImpl instance) =>
    <String, dynamic>{
      'login': instance.login,
      'id': instance.id,
      'node_id': instance.nodeId,
      'avatar_url': instance.avatarUrl,
      'gravatar_id': instance.gravatarId,
      'url': instance.url,
      'html_url': instance.htmlUrl,
      'type': instance.type,
      'site_admin': instance.siteAdmin,
    };
