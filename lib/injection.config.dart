// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'data/api_service/api_service.dart' as _i4;
import 'data/api_service/i_api_service.dart' as _i5;
import 'data/db_service/db_service.dart' as _i6;
import 'data/db_service/repository_db_service.dart' as _i7;
import 'data/repository/repo.dart' as _i9;
import 'presentation/bloc/internet/internet_bloc.dart' as _i8;
import 'presentation/bloc/repository_bloc/repository_bloc.dart' as _i10;
import 'util/endpoints/api_endpoints.dart' as _i3;

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  gh.singleton<_i3.ApiEndpoints>(_i3.ApiEndpoints());
  gh.lazySingleton<_i4.ApiService>(() => _i5.IApiService());
  gh.lazySingleton<_i6.DbService>(() => _i7.RepositoryDbService());
  gh.lazySingleton<_i8.InternetBloc>(() => _i8.InternetBloc());
  gh.lazySingleton<_i9.Repo>(() => _i9.IRepo(
        gh<_i4.ApiService>(),
        gh<_i6.DbService>(),
      ));
  gh.factory<_i10.RepositoryBloc>(() => _i10.RepositoryBloc(gh<_i9.Repo>()));
  return getIt;
}
