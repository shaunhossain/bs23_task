import 'package:bs23_task/domain/model/dropdown_model/dropdown_model.dart';

final List<DropdownModel> dropdownList = [
  DropdownModel(title: "Most stars", order: "desc", sort: "stars"),
  DropdownModel(title: "Fewest stars", order: "asc", sort: "stars"),
  DropdownModel(title: "Recently updated", order: "desc", sort: "updated"),
  DropdownModel(title: "Least recently updated", order: "asc", sort: "updated"),
];
