import 'package:injectable/injectable.dart';

@singleton
class ApiEndpoints {
  static const String baseUrl = "https://api.github.com";

  /// get all repository for a query element
  static searchRepositoriesUrl(
          {required String query, required int page, required int perPage}) =>
      "$baseUrl/search/repositories?q=$query&per_page=$perPage&page=$page";
  static searchWithFilterRepositoriesUrl(
          {required String query,
          required String order,
          required String sort,
          required int page,
          required int perPage}) =>
      "$baseUrl/search/repositories?q=$query&sort=$sort&order=$order&per_page=$perPage&page=$page";
}
