import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';



/// for google lato font
TextStyle latoH3Style(Color color) => GoogleFonts.lato(fontSize: 36, fontWeight: FontWeight.bold, color: color);
TextStyle latoH4Style(Color color) => GoogleFonts.lato(fontSize: 28, fontWeight: FontWeight.bold, color: color);
TextStyle latoH5Style(Color color) => GoogleFonts.lato(fontSize: 24, fontWeight: FontWeight.bold, color: color);
TextStyle latoH6Style(Color color) => GoogleFonts.lato(fontSize: 20, fontWeight: FontWeight.bold, color: color);
TextStyle latoH6MediumStyle(Color color) => GoogleFonts.lato(fontSize: 20, fontWeight: FontWeight.w500, color: color);
TextStyle latoH6RegularStyle(Color color) => GoogleFonts.lato(fontSize: 20, fontWeight: FontWeight.normal, color: color);
TextStyle latoBLStyle(Color color) => GoogleFonts.lato(fontSize: 17, fontWeight: FontWeight.bold, color: color);
TextStyle latoBLRegularStyle(Color color) => GoogleFonts.lato(fontSize: 17, fontWeight: FontWeight.normal, color: color);
TextStyle latoBLMediumStyle(Color color) => GoogleFonts.lato(fontSize: 17, fontWeight: FontWeight.w500, color: color);
TextStyle latoBMStyle(Color color) => GoogleFonts.lato(fontSize: 15, fontWeight: FontWeight.bold, color: color);
TextStyle latoBMStyleDefaultColor() => GoogleFonts.lato(fontSize: 15, fontWeight: FontWeight.bold);
TextStyle latoBMRegularStyle(Color color) => GoogleFonts.lato(fontSize: 15, fontWeight: FontWeight.normal, color: color);
TextStyle latoBMMediumStyle(Color color) => GoogleFonts.lato(fontSize: 15, fontWeight: FontWeight.w500, color: color);
TextStyle latoBSStyle(Color color) => GoogleFonts.lato(fontSize: 13, fontWeight: FontWeight.bold, color: color);
TextStyle latoBSRegularStyle(Color color) => GoogleFonts.lato(fontSize: 13, fontWeight: FontWeight.normal, color: color);
TextStyle latoBSMediumStyle(Color color) => GoogleFonts.lato(fontSize: 13, fontWeight: FontWeight.w500, color: color);
TextStyle latoCaptionStyle(Color color) => GoogleFonts.lato(fontSize: 11, fontWeight: FontWeight.bold, color: color);
TextStyle latoCaptionRegularStyle(Color color) => GoogleFonts.lato(fontSize: 11, fontWeight: FontWeight.normal, color: color);
TextStyle latoCaptionRegularCrossLineStyle(Color color) => GoogleFonts.lato(fontSize: 11, fontWeight: FontWeight.normal, color: color,decoration: TextDecoration.lineThrough,decorationColor: color);
TextStyle latoCaptionMediumStyle(Color color) => GoogleFonts.lato(fontSize: 11, fontWeight: FontWeight.w500, color: color);