 import 'dart:ui';

import 'package:flutter/material.dart';


/// default app color
enum AppColor{
  primary(Color(0xFF000421)),
  secondaryLight(Color(0xff191d37)),
  secondaryDark(Color(0xff0D112C)),
  unselectedLabel(Color(0xFFA0A0BA)),
  selectedLabel(Color(0xFFFFFFFF)),
  button(Color(0xff3483FF)),
  highlighter(Color(0xffFE346B));

  const AppColor(this.color);
  final Color color;
}





