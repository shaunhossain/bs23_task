enum ConnectionType {
  wifi("wifi"), mobile("mobile"), none("none");

  final String name;
  const ConnectionType(this.name);
}