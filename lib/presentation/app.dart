import 'package:bs23_task/injection.dart';
import 'package:bs23_task/presentation/bloc/internet/internet_bloc.dart';
import 'package:bs23_task/presentation/bloc/repository_bloc/repository_bloc.dart';
import 'package:bs23_task/presentation/ui/navigation/route.dart';
import 'package:bs23_task/util/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  const App({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => getIt<InternetBloc>()),
        BlocProvider(create: (context) => getIt<RepositoryBloc>()),
      ],
      child: MaterialApp.router(
        title: 'GitHub Repo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColor.primary.color),
          useMaterial3: true,
        ),
        debugShowCheckedModeBanner: false,
        routerConfig: router,
      ),
    );
  }
}
