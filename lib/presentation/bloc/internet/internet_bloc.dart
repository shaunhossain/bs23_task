

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'internet_state.dart';
part 'internet_event.dart';
part 'internet_bloc.freezed.dart';

@LazySingleton()
class InternetBloc extends Bloc<InternetEvent, InternetState> {

  InternetBloc() : super(const InternetState.initial()) {
    on<InternetEvent>((event, emit) {
      event.map(
          onConnected: (value) =>
              emit(const InternetState.connected("Connected.....")),
          onNotConnected: (value) =>
              emit(const InternetState.connected("Disconnected.....")));
    });
  }
}
