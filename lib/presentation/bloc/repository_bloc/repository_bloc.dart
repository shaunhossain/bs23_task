import 'package:bloc/bloc.dart';
import 'package:bs23_task/data/repository/repo.dart';
import 'package:bs23_task/domain/model/error_response/error_response.dart';
import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

part 'repository_event.dart';

part 'repository_state.dart';

part 'repository_bloc.freezed.dart';
part 'repository_bloc.g.dart';

@injectable
class RepositoryBloc extends Bloc<RepositoryEvent, RepositoryState> {
  final Repo repository;

  RepositoryBloc(this.repository) : super(const RepositoryState()) {
    int currentPage = 1;
    int itemPerPage = 10;
    on<RepositoryEvent>((event, emit) async {
      final hasConnected = await InternetConnectionChecker().hasConnection;
      await event.map(initialSearch: (_InitialSearch req) async {
        if (hasConnected) {
          if (state.hasReachedMax) return;
          if (state.status == RepositoryStatus.initial ||
              state.status == RepositoryStatus.error) {
            emit(state.copyWith(status: RepositoryStatus.loading));
            final result = await repository.getAllSearchRepo(
                query: req.query, page: currentPage, perPage: itemPerPage);
            return result.fold(
              (l) => emit(
                state.copyWith(
                  errorResponse: l,
                  hasReachedMax: false,
                  status: RepositoryStatus.error,
                ),
              ),
              (r) {
                if (r.first.items != null || r.first.items!.isNotEmpty) {
                  repository.clearAllLocalRepositories();
                  repository.saveRepositoryLocally(
                      repositories: r.first.items!);
                  emit(
                    state.copyWith(
                      repositories: r.first.items,
                      totalProductFound: r.first.totalCount ?? 0,
                      hasReachedMax: false,
                      status: RepositoryStatus.success,
                    ),
                  );
                }
              },
            );
          }
          currentPage = currentPage + 1;
          final result = await repository.getAllSearchRepo(
              query: req.query, page: currentPage, perPage: itemPerPage);
          return result.fold(
            (l) => emit(
              state.copyWith(
                errorResponse: l,
                repositories: state.repositories,
                hasReachedMax: false,
                status: RepositoryStatus.error,
              ),
            ),
            (r) {
              if (r.first.items == null || r.first.items!.isEmpty) {
                emit(state.copyWith(hasReachedMax: true));
              } else {
                repository.clearAllLocalRepositories();
                repository.saveRepositoryLocally(
                    repositories: List.of(state.repositories ?? [])
                      ..addAll(r.first.items!));
                emit(
                  state.copyWith(
                    repositories: List.of(state.repositories!)
                      ..addAll(r.first.items!),
                    totalProductFound: r.first.totalCount ?? 0,
                    hasReachedMax: false,
                  ),
                );
              }
            },
          );
        } else {
          final localRepositories = await repository.getAllLocalRepositories();
          emit(
            state.copyWith(
              repositories: localRepositories,
              totalProductFound: localRepositories.length,
              hasReachedMax: true,
              status: RepositoryStatus.success,
            ),
          );
        }
      }, refreshInitialSearch: (_RefreshInitialSearch ref) async {
        emit(const RepositoryState());
        await Future.delayed(const Duration(milliseconds: 100));
        add(RepositoryEvent.initialSearch(query: ref.query));
      }, filterSearch: (_FilterSearch req) async {
        if (hasConnected){
          if (state.hasReachedMax) return;
          if (state.status == RepositoryStatus.initial ||
              state.status == RepositoryStatus.error) {
            emit(state.copyWith(status: RepositoryStatus.loading));
            final result = await repository.getAllSearchFilterRepo(
                query: req.query,
                page: currentPage,
                perPage: itemPerPage,
                sort: req.sort,
                order: req.order);
            return result.fold(
                  (l) => emit(
                state.copyWith(
                  errorResponse: l,
                  hasReachedMax: false,
                  status: RepositoryStatus.error,
                ),
              ),
                  (r) {
                if (r.first.items != null || r.first.items!.isNotEmpty) {
                  repository.clearAllLocalRepositories();
                  repository.saveRepositoryLocally(
                      repositories: r.first.items!);
                  emit(
                    state.copyWith(
                      repositories: r.first.items,
                      totalProductFound: r.first.totalCount ?? 0,
                      hasReachedMax: false,
                      status: RepositoryStatus.success,
                    ),
                  );
                }
              },
            );
          }
          currentPage = currentPage + 1;
          final result = await repository.getAllSearchFilterRepo(
              query: req.query,
              page: currentPage,
              perPage: itemPerPage,
              sort: req.sort,
              order: req.order);
          return result.fold(
                (l) => emit(
              state.copyWith(
                errorResponse: l,
                repositories: state.repositories,
                hasReachedMax: false,
                status: RepositoryStatus.error,
              ),
            ),
                (r) {
              if (r.first.items == null || r.first.items!.isEmpty) {
                emit(state.copyWith(hasReachedMax: true));
              } else {
                repository.clearAllLocalRepositories();
                repository.saveRepositoryLocally(
                    repositories: List.of(state.repositories!)
                      ..addAll(r.first.items!));
                emit(
                  state.copyWith(
                    repositories: List.of(state.repositories!)
                      ..addAll(r.first.items!),
                    totalProductFound: r.first.totalCount ?? 0,
                    hasReachedMax: false,
                  ),
                );
              }
            },
          );
        }else{
          final localRepositories = await repository.getAllLocalRepositories();
          emit(
            state.copyWith(
              repositories: localRepositories,
              totalProductFound: localRepositories.length,
              hasReachedMax: true,
              status: RepositoryStatus.success,
            ),
          );
        }

      }, refreshFilterSearch: (_RefreshFilterSearch req) async {
        emit(const RepositoryState());
        await Future.delayed(const Duration(milliseconds: 100));
        add(RepositoryEvent.filterSearch(
            order: req.order, sort: req.sort, query: req.query));
      });
    });
  }
}
