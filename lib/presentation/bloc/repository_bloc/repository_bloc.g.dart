// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repository_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RepositoryStateImpl _$$RepositoryStateImplFromJson(
        Map<String, dynamic> json) =>
    _$RepositoryStateImpl(
      status: $enumDecodeNullable(_$RepositoryStatusEnumMap, json['status']) ??
          RepositoryStatus.initial,
      hasReachedMax: json['hasReachedMax'] as bool? ?? false,
      errorResponse: json['errorResponse'] == null
          ? null
          : ErrorResponse.fromJson(
              json['errorResponse'] as Map<String, dynamic>),
      totalProductFound: json['totalProductFound'] as int? ?? 0,
      repositories: (json['repositories'] as List<dynamic>?)
          ?.map((e) => Repository.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$RepositoryStateImplToJson(
        _$RepositoryStateImpl instance) =>
    <String, dynamic>{
      'status': _$RepositoryStatusEnumMap[instance.status]!,
      'hasReachedMax': instance.hasReachedMax,
      'errorResponse': instance.errorResponse,
      'totalProductFound': instance.totalProductFound,
      'repositories': instance.repositories,
    };

const _$RepositoryStatusEnumMap = {
  RepositoryStatus.initial: 'initial',
  RepositoryStatus.loading: 'loading',
  RepositoryStatus.success: 'success',
  RepositoryStatus.error: 'error',
};
