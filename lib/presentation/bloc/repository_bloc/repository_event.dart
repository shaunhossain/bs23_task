part of 'repository_bloc.dart';

@freezed
class RepositoryEvent with _$RepositoryEvent {
  const factory RepositoryEvent.initialSearch({required String query}) =
      _InitialSearch;

  const factory RepositoryEvent.refreshInitialSearch({required String query}) =
      _RefreshInitialSearch;

  const factory RepositoryEvent.filterSearch(
      {required String order,
      required String sort,
      required String query}) = _FilterSearch;

  const factory RepositoryEvent.refreshFilterSearch(
      {required String order,
      required String sort,
      required String query}) = _RefreshFilterSearch;
}
