part of 'repository_bloc.dart';

enum RepositoryStatus {
  initial,
  loading,
  success,
  error;

  bool get isInitial => this == RepositoryStatus.initial;
  bool get isLoading => this == RepositoryStatus.loading;
  bool get isSuccess => this == RepositoryStatus.success;
  bool get isFailure => this == RepositoryStatus.error;
}

@freezed
class RepositoryState with _$RepositoryState {
  const factory RepositoryState({
    @Default(RepositoryStatus.initial) RepositoryStatus status,
    @Default(false) bool hasReachedMax,
    ErrorResponse? errorResponse,
    @Default(0) int totalProductFound,
    List<Repository>? repositories,
  }) = _RepositoryState;

  factory RepositoryState.fromJson(Map<String, dynamic> json) =>
      _$RepositoryStateFromJson(json);
}