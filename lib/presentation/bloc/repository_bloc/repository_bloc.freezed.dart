// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'repository_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RepositoryEvent {
  String get query => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) initialSearch,
    required TResult Function(String query) refreshInitialSearch,
    required TResult Function(String order, String sort, String query)
        filterSearch,
    required TResult Function(String order, String sort, String query)
        refreshFilterSearch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? initialSearch,
    TResult? Function(String query)? refreshInitialSearch,
    TResult? Function(String order, String sort, String query)? filterSearch,
    TResult? Function(String order, String sort, String query)?
        refreshFilterSearch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? initialSearch,
    TResult Function(String query)? refreshInitialSearch,
    TResult Function(String order, String sort, String query)? filterSearch,
    TResult Function(String order, String sort, String query)?
        refreshFilterSearch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialSearch value) initialSearch,
    required TResult Function(_RefreshInitialSearch value) refreshInitialSearch,
    required TResult Function(_FilterSearch value) filterSearch,
    required TResult Function(_RefreshFilterSearch value) refreshFilterSearch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialSearch value)? initialSearch,
    TResult? Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult? Function(_FilterSearch value)? filterSearch,
    TResult? Function(_RefreshFilterSearch value)? refreshFilterSearch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialSearch value)? initialSearch,
    TResult Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult Function(_FilterSearch value)? filterSearch,
    TResult Function(_RefreshFilterSearch value)? refreshFilterSearch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RepositoryEventCopyWith<RepositoryEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryEventCopyWith<$Res> {
  factory $RepositoryEventCopyWith(
          RepositoryEvent value, $Res Function(RepositoryEvent) then) =
      _$RepositoryEventCopyWithImpl<$Res, RepositoryEvent>;
  @useResult
  $Res call({String query});
}

/// @nodoc
class _$RepositoryEventCopyWithImpl<$Res, $Val extends RepositoryEvent>
    implements $RepositoryEventCopyWith<$Res> {
  _$RepositoryEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? query = null,
  }) {
    return _then(_value.copyWith(
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InitialSearchImplCopyWith<$Res>
    implements $RepositoryEventCopyWith<$Res> {
  factory _$$InitialSearchImplCopyWith(
          _$InitialSearchImpl value, $Res Function(_$InitialSearchImpl) then) =
      __$$InitialSearchImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String query});
}

/// @nodoc
class __$$InitialSearchImplCopyWithImpl<$Res>
    extends _$RepositoryEventCopyWithImpl<$Res, _$InitialSearchImpl>
    implements _$$InitialSearchImplCopyWith<$Res> {
  __$$InitialSearchImplCopyWithImpl(
      _$InitialSearchImpl _value, $Res Function(_$InitialSearchImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? query = null,
  }) {
    return _then(_$InitialSearchImpl(
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InitialSearchImpl implements _InitialSearch {
  const _$InitialSearchImpl({required this.query});

  @override
  final String query;

  @override
  String toString() {
    return 'RepositoryEvent.initialSearch(query: $query)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialSearchImpl &&
            (identical(other.query, query) || other.query == query));
  }

  @override
  int get hashCode => Object.hash(runtimeType, query);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InitialSearchImplCopyWith<_$InitialSearchImpl> get copyWith =>
      __$$InitialSearchImplCopyWithImpl<_$InitialSearchImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) initialSearch,
    required TResult Function(String query) refreshInitialSearch,
    required TResult Function(String order, String sort, String query)
        filterSearch,
    required TResult Function(String order, String sort, String query)
        refreshFilterSearch,
  }) {
    return initialSearch(query);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? initialSearch,
    TResult? Function(String query)? refreshInitialSearch,
    TResult? Function(String order, String sort, String query)? filterSearch,
    TResult? Function(String order, String sort, String query)?
        refreshFilterSearch,
  }) {
    return initialSearch?.call(query);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? initialSearch,
    TResult Function(String query)? refreshInitialSearch,
    TResult Function(String order, String sort, String query)? filterSearch,
    TResult Function(String order, String sort, String query)?
        refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (initialSearch != null) {
      return initialSearch(query);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialSearch value) initialSearch,
    required TResult Function(_RefreshInitialSearch value) refreshInitialSearch,
    required TResult Function(_FilterSearch value) filterSearch,
    required TResult Function(_RefreshFilterSearch value) refreshFilterSearch,
  }) {
    return initialSearch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialSearch value)? initialSearch,
    TResult? Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult? Function(_FilterSearch value)? filterSearch,
    TResult? Function(_RefreshFilterSearch value)? refreshFilterSearch,
  }) {
    return initialSearch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialSearch value)? initialSearch,
    TResult Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult Function(_FilterSearch value)? filterSearch,
    TResult Function(_RefreshFilterSearch value)? refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (initialSearch != null) {
      return initialSearch(this);
    }
    return orElse();
  }
}

abstract class _InitialSearch implements RepositoryEvent {
  const factory _InitialSearch({required final String query}) =
      _$InitialSearchImpl;

  @override
  String get query;
  @override
  @JsonKey(ignore: true)
  _$$InitialSearchImplCopyWith<_$InitialSearchImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RefreshInitialSearchImplCopyWith<$Res>
    implements $RepositoryEventCopyWith<$Res> {
  factory _$$RefreshInitialSearchImplCopyWith(_$RefreshInitialSearchImpl value,
          $Res Function(_$RefreshInitialSearchImpl) then) =
      __$$RefreshInitialSearchImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String query});
}

/// @nodoc
class __$$RefreshInitialSearchImplCopyWithImpl<$Res>
    extends _$RepositoryEventCopyWithImpl<$Res, _$RefreshInitialSearchImpl>
    implements _$$RefreshInitialSearchImplCopyWith<$Res> {
  __$$RefreshInitialSearchImplCopyWithImpl(_$RefreshInitialSearchImpl _value,
      $Res Function(_$RefreshInitialSearchImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? query = null,
  }) {
    return _then(_$RefreshInitialSearchImpl(
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$RefreshInitialSearchImpl implements _RefreshInitialSearch {
  const _$RefreshInitialSearchImpl({required this.query});

  @override
  final String query;

  @override
  String toString() {
    return 'RepositoryEvent.refreshInitialSearch(query: $query)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RefreshInitialSearchImpl &&
            (identical(other.query, query) || other.query == query));
  }

  @override
  int get hashCode => Object.hash(runtimeType, query);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RefreshInitialSearchImplCopyWith<_$RefreshInitialSearchImpl>
      get copyWith =>
          __$$RefreshInitialSearchImplCopyWithImpl<_$RefreshInitialSearchImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) initialSearch,
    required TResult Function(String query) refreshInitialSearch,
    required TResult Function(String order, String sort, String query)
        filterSearch,
    required TResult Function(String order, String sort, String query)
        refreshFilterSearch,
  }) {
    return refreshInitialSearch(query);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? initialSearch,
    TResult? Function(String query)? refreshInitialSearch,
    TResult? Function(String order, String sort, String query)? filterSearch,
    TResult? Function(String order, String sort, String query)?
        refreshFilterSearch,
  }) {
    return refreshInitialSearch?.call(query);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? initialSearch,
    TResult Function(String query)? refreshInitialSearch,
    TResult Function(String order, String sort, String query)? filterSearch,
    TResult Function(String order, String sort, String query)?
        refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (refreshInitialSearch != null) {
      return refreshInitialSearch(query);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialSearch value) initialSearch,
    required TResult Function(_RefreshInitialSearch value) refreshInitialSearch,
    required TResult Function(_FilterSearch value) filterSearch,
    required TResult Function(_RefreshFilterSearch value) refreshFilterSearch,
  }) {
    return refreshInitialSearch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialSearch value)? initialSearch,
    TResult? Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult? Function(_FilterSearch value)? filterSearch,
    TResult? Function(_RefreshFilterSearch value)? refreshFilterSearch,
  }) {
    return refreshInitialSearch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialSearch value)? initialSearch,
    TResult Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult Function(_FilterSearch value)? filterSearch,
    TResult Function(_RefreshFilterSearch value)? refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (refreshInitialSearch != null) {
      return refreshInitialSearch(this);
    }
    return orElse();
  }
}

abstract class _RefreshInitialSearch implements RepositoryEvent {
  const factory _RefreshInitialSearch({required final String query}) =
      _$RefreshInitialSearchImpl;

  @override
  String get query;
  @override
  @JsonKey(ignore: true)
  _$$RefreshInitialSearchImplCopyWith<_$RefreshInitialSearchImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FilterSearchImplCopyWith<$Res>
    implements $RepositoryEventCopyWith<$Res> {
  factory _$$FilterSearchImplCopyWith(
          _$FilterSearchImpl value, $Res Function(_$FilterSearchImpl) then) =
      __$$FilterSearchImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String order, String sort, String query});
}

/// @nodoc
class __$$FilterSearchImplCopyWithImpl<$Res>
    extends _$RepositoryEventCopyWithImpl<$Res, _$FilterSearchImpl>
    implements _$$FilterSearchImplCopyWith<$Res> {
  __$$FilterSearchImplCopyWithImpl(
      _$FilterSearchImpl _value, $Res Function(_$FilterSearchImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? order = null,
    Object? sort = null,
    Object? query = null,
  }) {
    return _then(_$FilterSearchImpl(
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as String,
      sort: null == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as String,
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FilterSearchImpl implements _FilterSearch {
  const _$FilterSearchImpl(
      {required this.order, required this.sort, required this.query});

  @override
  final String order;
  @override
  final String sort;
  @override
  final String query;

  @override
  String toString() {
    return 'RepositoryEvent.filterSearch(order: $order, sort: $sort, query: $query)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FilterSearchImpl &&
            (identical(other.order, order) || other.order == order) &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.query, query) || other.query == query));
  }

  @override
  int get hashCode => Object.hash(runtimeType, order, sort, query);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FilterSearchImplCopyWith<_$FilterSearchImpl> get copyWith =>
      __$$FilterSearchImplCopyWithImpl<_$FilterSearchImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) initialSearch,
    required TResult Function(String query) refreshInitialSearch,
    required TResult Function(String order, String sort, String query)
        filterSearch,
    required TResult Function(String order, String sort, String query)
        refreshFilterSearch,
  }) {
    return filterSearch(order, sort, query);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? initialSearch,
    TResult? Function(String query)? refreshInitialSearch,
    TResult? Function(String order, String sort, String query)? filterSearch,
    TResult? Function(String order, String sort, String query)?
        refreshFilterSearch,
  }) {
    return filterSearch?.call(order, sort, query);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? initialSearch,
    TResult Function(String query)? refreshInitialSearch,
    TResult Function(String order, String sort, String query)? filterSearch,
    TResult Function(String order, String sort, String query)?
        refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (filterSearch != null) {
      return filterSearch(order, sort, query);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialSearch value) initialSearch,
    required TResult Function(_RefreshInitialSearch value) refreshInitialSearch,
    required TResult Function(_FilterSearch value) filterSearch,
    required TResult Function(_RefreshFilterSearch value) refreshFilterSearch,
  }) {
    return filterSearch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialSearch value)? initialSearch,
    TResult? Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult? Function(_FilterSearch value)? filterSearch,
    TResult? Function(_RefreshFilterSearch value)? refreshFilterSearch,
  }) {
    return filterSearch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialSearch value)? initialSearch,
    TResult Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult Function(_FilterSearch value)? filterSearch,
    TResult Function(_RefreshFilterSearch value)? refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (filterSearch != null) {
      return filterSearch(this);
    }
    return orElse();
  }
}

abstract class _FilterSearch implements RepositoryEvent {
  const factory _FilterSearch(
      {required final String order,
      required final String sort,
      required final String query}) = _$FilterSearchImpl;

  String get order;
  String get sort;
  @override
  String get query;
  @override
  @JsonKey(ignore: true)
  _$$FilterSearchImplCopyWith<_$FilterSearchImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RefreshFilterSearchImplCopyWith<$Res>
    implements $RepositoryEventCopyWith<$Res> {
  factory _$$RefreshFilterSearchImplCopyWith(_$RefreshFilterSearchImpl value,
          $Res Function(_$RefreshFilterSearchImpl) then) =
      __$$RefreshFilterSearchImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String order, String sort, String query});
}

/// @nodoc
class __$$RefreshFilterSearchImplCopyWithImpl<$Res>
    extends _$RepositoryEventCopyWithImpl<$Res, _$RefreshFilterSearchImpl>
    implements _$$RefreshFilterSearchImplCopyWith<$Res> {
  __$$RefreshFilterSearchImplCopyWithImpl(_$RefreshFilterSearchImpl _value,
      $Res Function(_$RefreshFilterSearchImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? order = null,
    Object? sort = null,
    Object? query = null,
  }) {
    return _then(_$RefreshFilterSearchImpl(
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as String,
      sort: null == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as String,
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$RefreshFilterSearchImpl implements _RefreshFilterSearch {
  const _$RefreshFilterSearchImpl(
      {required this.order, required this.sort, required this.query});

  @override
  final String order;
  @override
  final String sort;
  @override
  final String query;

  @override
  String toString() {
    return 'RepositoryEvent.refreshFilterSearch(order: $order, sort: $sort, query: $query)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RefreshFilterSearchImpl &&
            (identical(other.order, order) || other.order == order) &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.query, query) || other.query == query));
  }

  @override
  int get hashCode => Object.hash(runtimeType, order, sort, query);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RefreshFilterSearchImplCopyWith<_$RefreshFilterSearchImpl> get copyWith =>
      __$$RefreshFilterSearchImplCopyWithImpl<_$RefreshFilterSearchImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) initialSearch,
    required TResult Function(String query) refreshInitialSearch,
    required TResult Function(String order, String sort, String query)
        filterSearch,
    required TResult Function(String order, String sort, String query)
        refreshFilterSearch,
  }) {
    return refreshFilterSearch(order, sort, query);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? initialSearch,
    TResult? Function(String query)? refreshInitialSearch,
    TResult? Function(String order, String sort, String query)? filterSearch,
    TResult? Function(String order, String sort, String query)?
        refreshFilterSearch,
  }) {
    return refreshFilterSearch?.call(order, sort, query);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? initialSearch,
    TResult Function(String query)? refreshInitialSearch,
    TResult Function(String order, String sort, String query)? filterSearch,
    TResult Function(String order, String sort, String query)?
        refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (refreshFilterSearch != null) {
      return refreshFilterSearch(order, sort, query);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialSearch value) initialSearch,
    required TResult Function(_RefreshInitialSearch value) refreshInitialSearch,
    required TResult Function(_FilterSearch value) filterSearch,
    required TResult Function(_RefreshFilterSearch value) refreshFilterSearch,
  }) {
    return refreshFilterSearch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialSearch value)? initialSearch,
    TResult? Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult? Function(_FilterSearch value)? filterSearch,
    TResult? Function(_RefreshFilterSearch value)? refreshFilterSearch,
  }) {
    return refreshFilterSearch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialSearch value)? initialSearch,
    TResult Function(_RefreshInitialSearch value)? refreshInitialSearch,
    TResult Function(_FilterSearch value)? filterSearch,
    TResult Function(_RefreshFilterSearch value)? refreshFilterSearch,
    required TResult orElse(),
  }) {
    if (refreshFilterSearch != null) {
      return refreshFilterSearch(this);
    }
    return orElse();
  }
}

abstract class _RefreshFilterSearch implements RepositoryEvent {
  const factory _RefreshFilterSearch(
      {required final String order,
      required final String sort,
      required final String query}) = _$RefreshFilterSearchImpl;

  String get order;
  String get sort;
  @override
  String get query;
  @override
  @JsonKey(ignore: true)
  _$$RefreshFilterSearchImplCopyWith<_$RefreshFilterSearchImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

RepositoryState _$RepositoryStateFromJson(Map<String, dynamic> json) {
  return _RepositoryState.fromJson(json);
}

/// @nodoc
mixin _$RepositoryState {
  RepositoryStatus get status => throw _privateConstructorUsedError;
  bool get hasReachedMax => throw _privateConstructorUsedError;
  ErrorResponse? get errorResponse => throw _privateConstructorUsedError;
  int get totalProductFound => throw _privateConstructorUsedError;
  List<Repository>? get repositories => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RepositoryStateCopyWith<RepositoryState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryStateCopyWith<$Res> {
  factory $RepositoryStateCopyWith(
          RepositoryState value, $Res Function(RepositoryState) then) =
      _$RepositoryStateCopyWithImpl<$Res, RepositoryState>;
  @useResult
  $Res call(
      {RepositoryStatus status,
      bool hasReachedMax,
      ErrorResponse? errorResponse,
      int totalProductFound,
      List<Repository>? repositories});

  $ErrorResponseCopyWith<$Res>? get errorResponse;
}

/// @nodoc
class _$RepositoryStateCopyWithImpl<$Res, $Val extends RepositoryState>
    implements $RepositoryStateCopyWith<$Res> {
  _$RepositoryStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? hasReachedMax = null,
    Object? errorResponse = freezed,
    Object? totalProductFound = null,
    Object? repositories = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as RepositoryStatus,
      hasReachedMax: null == hasReachedMax
          ? _value.hasReachedMax
          : hasReachedMax // ignore: cast_nullable_to_non_nullable
              as bool,
      errorResponse: freezed == errorResponse
          ? _value.errorResponse
          : errorResponse // ignore: cast_nullable_to_non_nullable
              as ErrorResponse?,
      totalProductFound: null == totalProductFound
          ? _value.totalProductFound
          : totalProductFound // ignore: cast_nullable_to_non_nullable
              as int,
      repositories: freezed == repositories
          ? _value.repositories
          : repositories // ignore: cast_nullable_to_non_nullable
              as List<Repository>?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ErrorResponseCopyWith<$Res>? get errorResponse {
    if (_value.errorResponse == null) {
      return null;
    }

    return $ErrorResponseCopyWith<$Res>(_value.errorResponse!, (value) {
      return _then(_value.copyWith(errorResponse: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$RepositoryStateImplCopyWith<$Res>
    implements $RepositoryStateCopyWith<$Res> {
  factory _$$RepositoryStateImplCopyWith(_$RepositoryStateImpl value,
          $Res Function(_$RepositoryStateImpl) then) =
      __$$RepositoryStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {RepositoryStatus status,
      bool hasReachedMax,
      ErrorResponse? errorResponse,
      int totalProductFound,
      List<Repository>? repositories});

  @override
  $ErrorResponseCopyWith<$Res>? get errorResponse;
}

/// @nodoc
class __$$RepositoryStateImplCopyWithImpl<$Res>
    extends _$RepositoryStateCopyWithImpl<$Res, _$RepositoryStateImpl>
    implements _$$RepositoryStateImplCopyWith<$Res> {
  __$$RepositoryStateImplCopyWithImpl(
      _$RepositoryStateImpl _value, $Res Function(_$RepositoryStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? hasReachedMax = null,
    Object? errorResponse = freezed,
    Object? totalProductFound = null,
    Object? repositories = freezed,
  }) {
    return _then(_$RepositoryStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as RepositoryStatus,
      hasReachedMax: null == hasReachedMax
          ? _value.hasReachedMax
          : hasReachedMax // ignore: cast_nullable_to_non_nullable
              as bool,
      errorResponse: freezed == errorResponse
          ? _value.errorResponse
          : errorResponse // ignore: cast_nullable_to_non_nullable
              as ErrorResponse?,
      totalProductFound: null == totalProductFound
          ? _value.totalProductFound
          : totalProductFound // ignore: cast_nullable_to_non_nullable
              as int,
      repositories: freezed == repositories
          ? _value._repositories
          : repositories // ignore: cast_nullable_to_non_nullable
              as List<Repository>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RepositoryStateImpl implements _RepositoryState {
  const _$RepositoryStateImpl(
      {this.status = RepositoryStatus.initial,
      this.hasReachedMax = false,
      this.errorResponse,
      this.totalProductFound = 0,
      final List<Repository>? repositories})
      : _repositories = repositories;

  factory _$RepositoryStateImpl.fromJson(Map<String, dynamic> json) =>
      _$$RepositoryStateImplFromJson(json);

  @override
  @JsonKey()
  final RepositoryStatus status;
  @override
  @JsonKey()
  final bool hasReachedMax;
  @override
  final ErrorResponse? errorResponse;
  @override
  @JsonKey()
  final int totalProductFound;
  final List<Repository>? _repositories;
  @override
  List<Repository>? get repositories {
    final value = _repositories;
    if (value == null) return null;
    if (_repositories is EqualUnmodifiableListView) return _repositories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'RepositoryState(status: $status, hasReachedMax: $hasReachedMax, errorResponse: $errorResponse, totalProductFound: $totalProductFound, repositories: $repositories)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RepositoryStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.hasReachedMax, hasReachedMax) ||
                other.hasReachedMax == hasReachedMax) &&
            (identical(other.errorResponse, errorResponse) ||
                other.errorResponse == errorResponse) &&
            (identical(other.totalProductFound, totalProductFound) ||
                other.totalProductFound == totalProductFound) &&
            const DeepCollectionEquality()
                .equals(other._repositories, _repositories));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      status,
      hasReachedMax,
      errorResponse,
      totalProductFound,
      const DeepCollectionEquality().hash(_repositories));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RepositoryStateImplCopyWith<_$RepositoryStateImpl> get copyWith =>
      __$$RepositoryStateImplCopyWithImpl<_$RepositoryStateImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RepositoryStateImplToJson(
      this,
    );
  }
}

abstract class _RepositoryState implements RepositoryState {
  const factory _RepositoryState(
      {final RepositoryStatus status,
      final bool hasReachedMax,
      final ErrorResponse? errorResponse,
      final int totalProductFound,
      final List<Repository>? repositories}) = _$RepositoryStateImpl;

  factory _RepositoryState.fromJson(Map<String, dynamic> json) =
      _$RepositoryStateImpl.fromJson;

  @override
  RepositoryStatus get status;
  @override
  bool get hasReachedMax;
  @override
  ErrorResponse? get errorResponse;
  @override
  int get totalProductFound;
  @override
  List<Repository>? get repositories;
  @override
  @JsonKey(ignore: true)
  _$$RepositoryStateImplCopyWith<_$RepositoryStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
