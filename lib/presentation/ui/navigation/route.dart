
import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:bs23_task/presentation/ui/navigation/page_name.dart';
import 'package:bs23_task/presentation/ui/page/detail_page/detail_page.dart';
import 'package:bs23_task/presentation/ui/page/home_page/home_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

final _rootNavigatorKey = GlobalKey<NavigatorState>();

final GoRouter router = GoRouter(
  navigatorKey: _rootNavigatorKey,
  initialLocation: PagesName.homePage.path,
  routes: [
    GoRoute(
      parentNavigatorKey: _rootNavigatorKey,
      path: PagesName.homePage.path,
      builder: (BuildContext context, GoRouterState state) {
        return const HomePage();
      },
    ),
    GoRoute(
      parentNavigatorKey: _rootNavigatorKey,
      path: PagesName.detailPage.path,
      pageBuilder: (context, state) {
        final repository = state.extra! as Repository;
        return NoTransitionPage(
          child: DetailPage(repository: repository,),
        );
      },
    ),
  ],
);
