enum PagesName{
  homePage("Home Page","/"),
  detailPage("Detail Page","/detail");



  const PagesName(this.title,this.path);
  final String title;
  final String path;
}