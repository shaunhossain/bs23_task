import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:bs23_task/util/app_colors.dart';
import 'package:bs23_task/util/size_config.dart';
import 'package:bs23_task/util/styles.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({super.key, required this.repository});
  final Repository repository;

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.repository.fullName ?? "",style: latoH6Style(Colors.black),),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            Card(
              elevation: 0.3,
              color: Colors.grey.shade100,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ClipRRect(
                            borderRadius: BorderRadius.circular(16),
                            child: CachedNetworkImage(
                              width: 82,
                              height: 82,
                              imageUrl: widget.repository.owner?.avatarUrl ?? "",
                              fit: BoxFit.fill,
                              placeholder: (context, url) => const Center(
                                  child: SizedBox(
                                      width: 16,
                                      height: 16,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 1,
                                      ))),
                              errorWidget: (context, url, error) => Icon(
                                Icons.error,
                                color: AppColor.highlighter.color,
                              ),
                            )),
                        Container(
                          margin: const EdgeInsets.only(left: 12.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.repository.name ??"",
                                style: latoBMRegularStyle(Colors.black),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              const SizedBox(height: 4,),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 8,
                                        height: 8,
                                        margin: const EdgeInsets.only(right: 6),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: widget.repository.language!.toLowerCase().contains("dart")
                                                ? Colors.lightBlueAccent
                                                : Colors.redAccent),
                                      ),
                                      Text(
                                        widget.repository.language ?? "",
                                        style: latoCaptionMediumStyle(Colors.black),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 2,
                                        height: 2,
                                        margin: const EdgeInsets.symmetric(horizontal: 6),
                                        decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.black),
                                      ),
                                      const Icon(Icons.star_border,size: 14,),
                                      const SizedBox(width: 2,),
                                      Text(
                                        widget.repository.stargazersCount! > 1000 ? "${(widget.repository.stargazersCount! / 1000).round()}k" : "${widget.repository.stargazersCount}",
                                        style: latoCaptionMediumStyle(Colors.black),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 2,
                                        height: 2,
                                        margin: const EdgeInsets.symmetric(horizontal: 6),
                                        decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.black),
                                      ),
                                      const Icon(Icons.account_tree_outlined,size: 14,),
                                      const SizedBox(width: 2,),
                                      Text(
                                        widget.repository.forks! > 1000 ? "${(widget.repository.forks! / 1000).round()}k" : "${widget.repository.forks}",
                                        style: latoCaptionMediumStyle(Colors.black),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 2,
                                        height: 2,
                                        margin: const EdgeInsets.symmetric(horizontal: 6),
                                        decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.black),
                                      ),
                                      const Icon(Icons.remove_red_eye_outlined,size: 14,),
                                      const SizedBox(width: 2,),
                                      Text(
                                        widget.repository.watchers! > 1000 ? "${(widget.repository.watchers! / 1000).round()}k" : "${widget.repository.watchers}",
                                        style: latoCaptionMediumStyle(Colors.black),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(height: 4,),
                              Text(
                                "Created at : ${DateFormat('MM-dd-yy HH:mm').format(widget.repository.createdAt!)}",
                                style: latoCaptionRegularStyle(Colors.black),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              const SizedBox(height: 4,),
                              Text(
                                "Updated at : ${DateFormat('MM-dd-yy HH:mm').format(widget.repository.updatedAt!)}",
                                style: latoCaptionRegularStyle(Colors.black),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),

                            ],
                          ),
                        )
                      ],
                    ),
                    ConstrainedBox(
                      constraints: const BoxConstraints(
                        maxWidth: double.maxFinite
                      ),
                      child: GridView.builder(
                        shrinkWrap: true,
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          mainAxisSpacing: 4.0,
                          crossAxisSpacing: 4.0,
                          childAspectRatio: 16/5
                        ),
                        padding: const EdgeInsets.only(top: 8.0,bottom: 8),
                        itemCount: widget.repository.topics?.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: const EdgeInsets.only(
                              right: 6,
                            ),
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, top: 3, bottom: 3),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(16),
                                color: Colors.lightBlueAccent.shade100),
                            child: FittedBox(
                              child: Text(
                                widget.repository.topics![index],
                                style: latoCaptionStyle(Colors.blueAccent),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    const SizedBox(height: 4,),
                    Text(
                      widget.repository.description ??"",
                      style: latoBMRegularStyle(Colors.black),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
