import 'dart:async';

import 'package:bs23_task/domain/model/dropdown_model/dropdown_model.dart';
import 'package:bs23_task/presentation/bloc/internet/internet_bloc.dart';
import 'package:bs23_task/presentation/bloc/repository_bloc/repository_bloc.dart';
import 'package:bs23_task/presentation/ui/navigation/page_name.dart';
import 'package:bs23_task/presentation/ui/widget/error_indicator.dart';
import 'package:bs23_task/presentation/ui/widget/home/custom_dropdown_widget.dart';
import 'package:bs23_task/presentation/ui/widget/home/custom_retry_widget.dart';
import 'package:bs23_task/presentation/ui/widget/home/repository_item.dart';
import 'package:bs23_task/presentation/ui/widget/loading_indicator_widget.dart';
import 'package:bs23_task/util/app_colors.dart';
import 'package:bs23_task/util/connection_type.dart';
import 'package:bs23_task/util/dropdown_value.dart';
import 'package:bs23_task/util/size_config.dart';
import 'package:bs23_task/util/styles.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final ScrollController _scrollController;
  DropdownModel? selectedFilterItem;
  StreamSubscription? subscriptions;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_onScroll);
    context
        .read<RepositoryBloc>()
        .add(const RepositoryEvent.refreshInitialSearch(query: "Flutter"));
    subscriptions = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result.name == ConnectionType.wifi.name ||
          result.name == ConnectionType.mobile.name) {
        context.read<InternetBloc>().add(const InternetEvent.onConnected());
      } else {
        context.read<InternetBloc>().add(const InternetEvent.onNotConnected());
      }
    });

    Timer.periodic(
        const Duration(minutes: 30),
        (_) => context
            .read<RepositoryBloc>()
            .add(const RepositoryEvent.refreshInitialSearch(query: "Flutter")));
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    subscriptions?.cancel();
    super.dispose();
  }

  void _onScroll() {
    if (!_scrollController.hasClients) return;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (currentScroll == maxScroll) {
      context
          .read<RepositoryBloc>()
          .add(const RepositoryEvent.initialSearch(query: "Flutter"));

      if (selectedFilterItem == null) {
        context
            .read<RepositoryBloc>()
            .add(const RepositoryEvent.initialSearch(query: "Flutter"));
      } else {
        context.read<RepositoryBloc>().add(RepositoryEvent.filterSearch(
            sort: selectedFilterItem!.sort,
            order: selectedFilterItem!.order,
            query: "Flutter"));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "GitHub Repository",
          style: latoH6Style(Colors.black),
        ),
        actions: [
          CustomDropdown(
            dropdownList: dropdownList,
            onSelected: (value) {
              context.read<RepositoryBloc>().add(
                  RepositoryEvent.refreshFilterSearch(
                      sort: value.sort, order: value.order, query: "Flutter"));
              setState(() {
                selectedFilterItem = value;
              });
            },
          ),
        ],
      ),
      body: BlocListener<InternetBloc, InternetState>(
          listener: (context, state) {
            state.maybeWhen(
              orElse: () {},
              connected: (message) {
                context.read<RepositoryBloc>().add(
                    const RepositoryEvent.refreshInitialSearch(
                        query: "Flutter"));
              },
            );
          },
          child: RefreshIndicator(
            color: AppColor.highlighter.color,
            backgroundColor: Colors.white,
            onRefresh: () async {
              context.read<RepositoryBloc>().add(
                  const RepositoryEvent.refreshInitialSearch(query: "Flutter"));
            },
            child: CustomScrollView(
              controller: _scrollController,
              physics: const ScrollPhysics(),
              slivers: [
                SliverPadding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
                  sliver: BlocBuilder<RepositoryBloc, RepositoryState>(
                    builder: (context, state) {
                      if (state.status == RepositoryStatus.success) {
                        if (state.totalProductFound > 0) {
                          return SliverToBoxAdapter(
                            child: Text(
                              state.totalProductFound >= 1000
                                  ? "${(state.totalProductFound / 1000).round()}k results"
                                  : "${state.totalProductFound} results",
                              style: latoBLStyle(Colors.grey),
                            ),
                          );
                        }
                      }
                      return const SliverToBoxAdapter();
                    },
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  sliver: BlocBuilder<RepositoryBloc, RepositoryState>(
                      builder: (context, state) {
                    switch (state.status) {
                      case RepositoryStatus.initial:
                        return const SliverToBoxAdapter();
                      case RepositoryStatus.loading:
                        return const SliverToBoxAdapter(
                            child: LoadingIndicatorWidget());
                      case RepositoryStatus.success:
                        if (state.repositories!.isNotEmpty) {
                          return SliverList(
                              delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                            return index >= state.repositories!.length
                                ? const LoadingIndicatorWidget()
                                : RepositoryItem(
                                    ownerAvatarUrl: state.repositories![index]
                                            .owner?.avatarUrl ??
                                        "",
                                    fulTitle:
                                        state.repositories?[index].fullName ??
                                            "",
                                    description: state
                                            .repositories?[index].description ??
                                        "",
                                    language:
                                        state.repositories?[index].language ??
                                            "",
                                    topics:
                                        state.repositories![index].topics ?? [],
                                    onTap: () {
                                      context.push(PagesName.detailPage.path,
                                          extra: state.repositories?[index]);
                                    },
                                    stars: state.repositories?[index]
                                            .stargazersCount ??
                                        0,
                                    lastUpdated:
                                        state.repositories![index].updatedAt!,
                                  );
                          },
                                  childCount: state.hasReachedMax
                                      ? state.repositories?.length
                                      : state.repositories!.length + 1));
                        }
                        return SliverToBoxAdapter(
                            child: CustomRetryWidget(
                          message: "Fail to load repository",
                          onRetry: () {
                            context.read<RepositoryBloc>().add(
                                const RepositoryEvent.initialSearch(
                                    query: "Flutter"));
                          },
                        ));
                      case RepositoryStatus.error:
                        return SliverToBoxAdapter(
                          child: ErrorIndicator(
                            message: state.errorResponse?.message ?? "",
                          ),
                        );
                    }
                  }),
                ),
              ],
            ),
          )),
    );
  }
}
