import 'package:bs23_task/domain/model/dropdown_model/dropdown_model.dart';
import 'package:bs23_task/util/size_config.dart';
import 'package:bs23_task/util/styles.dart';
import 'package:flutter/material.dart';

class CustomDropdown extends StatefulWidget {
  const CustomDropdown(
      {super.key, required this.dropdownList, this.onSelected});

  final List<DropdownModel> dropdownList;
  final ValueChanged<DropdownModel>? onSelected;

  @override
  State<CustomDropdown> createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  DropdownModel? selectedItem;

  void onCurrencySelect(DropdownModel? value) {
    setState(() {
      selectedItem = value;
    });
    widget.onSelected?.call(value!);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: const EdgeInsets.only(right: 16),
      child: ConstrainedBox(
        constraints: BoxConstraints(
            minWidth: SizeConfig.width! * 0.2,
            maxWidth: SizeConfig.width! * 0.3,
            maxHeight: 32),
        child: DropdownButton<DropdownModel>(
          padding: const EdgeInsets.only(left: 8, right: 8),
          isExpanded: true,
          hint: Text(
            selectedItem?.title ?? "",
            style: latoBSMediumStyle(Colors.grey.shade700),
          ),
          value: selectedItem,
          icon: const Icon(
            Icons.filter_list,
            size: 16,
          ),
          elevation: 0,
          dropdownColor: Colors.lightBlueAccent,
          borderRadius: BorderRadius.circular(10),
          underline: const SizedBox(),
          style: latoBMMediumStyle(Colors.white),
          onChanged: onCurrencySelect,
          items: widget.dropdownList.map<DropdownMenuItem<DropdownModel>>(
            (DropdownModel value) {
              return DropdownMenuItem<DropdownModel>(
                value: value,
                child: Text(
                  value.title,
                  style: latoBSMediumStyle(Colors.black),
                  maxLines: 1,
                ),
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}
