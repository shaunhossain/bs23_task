import 'package:bs23_task/util/app_colors.dart';
import 'package:bs23_task/util/size_config.dart';
import 'package:bs23_task/util/styles.dart';
import 'package:flutter/material.dart';

class CustomRetryWidget extends StatelessWidget {
  const CustomRetryWidget({super.key, required this.message, required this.onRetry});
  final String message;
  final Function() onRetry;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.maxFinite,
        height: SizeConfig.height! * 0.6,
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              message,
              style: latoBLRegularStyle(
                  AppColor.highlighter.color),
            ),
            ElevatedButton(
                onPressed: onRetry,
                child: Text(
                  "Retry",
                  style: latoBLMediumStyle(
                      AppColor.button.color),
                ))
          ],
        ));
  }
}
