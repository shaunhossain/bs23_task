import 'package:bs23_task/util/app_colors.dart';
import 'package:bs23_task/util/size_config.dart';
import 'package:bs23_task/util/styles.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter/material.dart';

class RepositoryItem extends StatelessWidget {
  const RepositoryItem(
      {super.key,
      required this.ownerAvatarUrl,
      required this.fulTitle,
      required this.description,
      required this.language,
      required this.topics, required this.onTap, required this.stars, required this.lastUpdated});
  final String ownerAvatarUrl;
  final String fulTitle;
  final String description;
  final String language;
  final int stars;
  final DateTime lastUpdated;
  final List<String> topics;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return InkWell(
      onTap: onTap,
      child: Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.all(16),
          margin: const EdgeInsets.symmetric(vertical: 8),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              border: Border.all(color: Colors.grey, width: 1)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: CachedNetworkImage(
                        width: 24,
                        height: 24,
                        imageUrl: ownerAvatarUrl,
                        fit: BoxFit.fill,
                        placeholder: (context, url) => const Center(
                            child: SizedBox(
                                width: 16,
                                height: 16,
                                child: CircularProgressIndicator(
                                  strokeWidth: 1,
                                ))),
                        errorWidget: (context, url, error) => Icon(
                          Icons.error,
                          color: AppColor.highlighter.color,
                        ),
                      )),
                  const SizedBox(
                    width: 16,
                  ),
                  SizedBox(
                    width: SizeConfig.width! * 0.7,
                    child: Text(
                      fulTitle,
                      style: latoBMRegularStyle(Colors.black),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              if(description.isNotEmpty)
              const SizedBox(
                height: 8,
              ),
              if(description.isNotEmpty)
              Text(
                description,
                style: latoBSMediumStyle(Colors.black),
              ),
              if(topics.isNotEmpty)
              Padding(
                padding: const EdgeInsets.only(top: 12.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: topics.map((item) {
                    if (item.toLowerCase().contains("ios") ||
                        item.toLowerCase().contains("android") ||
                        item.toLowerCase().contains("cross-platform")) {
                      return Container(
                        margin: const EdgeInsets.only(
                          right: 6,
                        ),
                        padding: const EdgeInsets.only(
                            left: 8, right: 8, top: 3, bottom: 3),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(16),
                            color: Colors.lightBlueAccent.shade100),
                        child: Text(
                          item,
                          style: latoCaptionRegularStyle(
                              Colors.blueAccent.shade700),
                        ),
                      );
                    }
                    return const SizedBox();
                  }).toList(),
                ),
              ),
              if(language.isNotEmpty)
              const SizedBox(
                height: 12,
              ),
              if(language.isNotEmpty)
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 8,
                        height: 8,
                        margin: const EdgeInsets.only(right: 6),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: language.toLowerCase().contains("dart")
                                ? Colors.lightBlueAccent
                                : Colors.redAccent),
                      ),
                      Text(
                        language,
                        style: latoCaptionMediumStyle(Colors.black),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 2,
                        height: 2,
                        margin: const EdgeInsets.symmetric(horizontal: 6),
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.black),
                      ),
                      const Icon(Icons.star_border,size: 14,),
                      const SizedBox(width: 2,),
                      Text(
                        stars > 1000 ? "${(stars / 1000).round()}k" : "$stars",
                        style: latoCaptionMediumStyle(Colors.black),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 2,
                        height: 2,
                        margin: const EdgeInsets.symmetric(horizontal: 6),
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.black),
                      ),
                      Text("Updated ${timeago.format(lastUpdated)}",
                        style: latoCaptionMediumStyle(Colors.black),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
