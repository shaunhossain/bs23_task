import 'package:bs23_task/util/app_colors.dart';
import 'package:bs23_task/util/styles.dart';
import 'package:flutter/material.dart';

class ErrorIndicator extends StatelessWidget {
  const ErrorIndicator({super.key, required this.message});
  final String message;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text(
      message,
      style: latoBMStyle(AppColor.highlighter.color),
    ));
  }
}
