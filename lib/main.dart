import 'package:bs23_task/domain/model/repository_response/repository_response.dart';
import 'package:bs23_task/presentation/app.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';

import 'injection.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies(environment: Environment.prod);
  await Hive.initFlutter();
  Hive.registerAdapter(RepositoryResponseAdapter());
  Hive.registerAdapter(RepositoryAdapter());
  Hive.registerAdapter(LicenseAdapter());
  Hive.registerAdapter(OwnerAdapter());
  runApp(const App());
}

